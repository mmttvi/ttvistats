/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import ch.sperr.ttvistats.data.Series;
import ch.sperr.ttvistats.data.TeamData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * TeamCountHelperTest
 */
class TeamCountHelperTest {

   private static List<TeamData> largerTestSet;

   private TeamCountHelper teamCountHelper;


   @BeforeAll
   static void initTestSet() throws IOException, URISyntaxException {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource("ttvi-data-21-22-2023-01-16.gz")).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (
            InputStreamReader inputStreamReader = new InputStreamReader(
                  new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
            Jsonb jsonb = JsonbBuilder.create()) {
         List<TeamData> unfilteredData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);
         largerTestSet = unfilteredData.stream()
               .filter(t -> Series.HERREN.equals(t.series))
               .collect(Collectors.toList());

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @BeforeEach
   void initTeamCountHelper() {
      teamCountHelper = new TeamCountHelper(largerTestSet, 2);
   }

   @Test
   @DisplayName("Return correct count for existing club")
   void getTeamCount001() {
      assertThat(teamCountHelper.getTeamCount("Baar", 2), is(2));
   }

   @Test
   @DisplayName("Return zero count for non existing club")
   void getTeamCount002() {
      assertThat(teamCountHelper.getTeamCount("Some Weird Club", 2), is(0));
   }

   @Test
   @DisplayName("Return true for Test of new Team in 1st league for Baar")
   void isTeamOfClubAllowedInLeague001() {
      assertThat(teamCountHelper.getTeamCount("Baar", 1), is(1));
      assertThat(teamCountHelper.isTeamOfClubAllowedInLeague("Baar", 1), is(true));
   }

   @Test
   @DisplayName("Return false for Test of new Team in 1st league for Kriens after adding a second")
   void isTeamOfClubAllowedInLeague002() {
      teamCountHelper.addAdditionalTeamToLeague("Kriens", 1);
      assertThat(teamCountHelper.getTeamCount("Kriens", 1), is(2));
      assertThat(teamCountHelper.isTeamOfClubAllowedInLeague("Kriens", 1), is(false));
   }

   @Test
   @DisplayName("Add an additional Team to League where club already has one")
   void addAdditionalTeamToLeague001() {
      assertThat(teamCountHelper.getTeamCount("Kriens", 1), is(1));
      teamCountHelper.addAdditionalTeamToLeague("Kriens", 1);
      assertThat(teamCountHelper.getTeamCount("Kriens", 1), is(2));
   }

   @Test
   @DisplayName("Add an additional Team to League where club has none")
   void addAdditionalTeamToLeague002() {
      assertThat(teamCountHelper.getTeamCount("SomeClub", 1), is(0));
      teamCountHelper.addAdditionalTeamToLeague("SomeClub", 1);
      assertThat(teamCountHelper.getTeamCount("SomeClub", 1), is(1));
   }

   @Test
   @DisplayName("Move Team from 2nd to 1st League")
   void moveTeam001() {
      assertThat(teamCountHelper.getTeamCount("Baar", 1), is(1));
      assertThat(teamCountHelper.getTeamCount("Baar", 2), is(2));
      teamCountHelper.moveTeam("Baar", 2, 1);
      assertThat(teamCountHelper.getTeamCount("Baar", 1), is(2));
      assertThat(teamCountHelper.getTeamCount("Baar", 2), is(1));
   }

   @Test
   @DisplayName("get conflicting club names until all is all right")
   void getConflictingTeams () {
      teamCountHelper.addAdditionalTeamToLeague("Kriens", 1);
      teamCountHelper.addAdditionalTeamToLeague("Kriens", 1);
      teamCountHelper.addAdditionalTeamToLeague("Baar", 1);
      teamCountHelper.addAdditionalTeamToLeague("Baar", 1);
      teamCountHelper.addAdditionalTeamToLeague("Baar", 1);
      assertThat(teamCountHelper.getTeamCount("Kriens", 1), is(3));
      assertThat(teamCountHelper.getTeamCount("Baar", 1), is(4));

      Optional<String> nextCompromisingTeamInLeague = teamCountHelper.getFirstCompromisingTeamInLeague(1);
      while (nextCompromisingTeamInLeague.isPresent()) {
         teamCountHelper.moveTeam(nextCompromisingTeamInLeague.get(), 1, 2);
         nextCompromisingTeamInLeague = teamCountHelper.getFirstCompromisingTeamInLeague(1);
      }
      assertThat(teamCountHelper.getTeamCount("Kriens", 1), is(2));
      assertThat(teamCountHelper.getTeamCount("Baar", 1), is(2));
   }

   @Test
   @DisplayName("get correct team count for 2nd league")
   void getLeagueTeamCount() {
      assertThat(teamCountHelper.getLeagueTeamCount(2), is(15));
   }

}