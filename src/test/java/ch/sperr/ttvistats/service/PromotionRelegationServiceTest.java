/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import ch.sperr.ttvistats.data.Series;
import ch.sperr.ttvistats.data.TeamData;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * PromotionRelegationServiceTest
 *
 */
@QuarkusTest
class PromotionRelegationServiceTest {


   private static final String TEST_DATA_FILE = "ttvi-data-21-22-2023-01-16.gz";

   @Inject
   PromotionRelegationService service;
   @Test
   @DisplayName("Standard Promotions and Relegations")
   void calculateStandardPromotionAndRelegation() throws URISyntaxException, IOException {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         List<TeamData> regularPromotions = teamData.stream().filter(l -> l.isPromotedRegularly)
               .collect(Collectors.toList());

         assertThat(regularPromotions.size(), is(9));

         List<TeamData> allowedForNationalLeaguePromotion = teamData.stream().filter(l -> l.isAllowedForNationalLeaguePromotion)
               .collect(Collectors.toList());
         assertThat(allowedForNationalLeaguePromotion.size(), is(2));

         List<TeamData> additionalPromotion = teamData.stream().filter(l -> l.isPromotedAdditional)
               .collect(Collectors.toList());
         assertThat(additionalPromotion.size(), is(8));

         String resultJson = jsonb.toJson(teamData);

         System.out.println(resultJson);

         URI resultUri = Objects.requireNonNull(PromotionRelegationService.class.getResource("ttvi-data-21-22-2022-12-08-first-test-result.txt")).toURI();
         String expectedJson = Files.readString(Paths.get(resultUri));

         assertThat(resultJson, is(expectedJson));


      } catch (Exception e) {
         throw new TtviStatsException(e);
      }

   }


   @Test
   @DisplayName("Teams with denied Promotion don't get promoted")
   void promotionDenied () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData baarIv = getTeamByName(teamData, "Baar IV").orElseThrow();
         baarIv.isPromotionDeclinedByClub = true;

         TeamData zugV = getTeamByName(teamData, "Zug V").orElseThrow();
         zugV.isPromotionDeclinedByClub = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(baarIv.isPromotedRegularly, is(false));
         assertThat(baarIv.isPromotedAdditional, is(false));

         TeamData kriensVi = getTeamByName(teamData, "Kriens VI").orElseThrow();
         assertThat(kriensVi.isPromotedRegularly, is(true));

         assertThat(zugV.isPromotedRegularly, is(false));
         assertThat(zugV.isPromotedAdditional, is(false));

         TeamData rothenburgVi = getTeamByName(teamData, "Rothenburg VI").orElseThrow();
         assertThat(rothenburgVi.isPromotedAdditional, is(true));

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Group with not enough promotable teams")
   void tooManyPromotionsDenied () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData baarIv = getTeamByName(teamData, "Baar IV").orElseThrow();
         baarIv.isPromotionDeclinedByClub = true;

         TeamData kriensVi = getTeamByName(teamData, "Kriens VI").orElseThrow();
         kriensVi.isPromotionDeclinedByClub = true;

         TeamData sarnenIi = getTeamByName(teamData, "Sarnen II").orElseThrow();
         sarnenIi.isPromotionDeclinedByClub = true;

         TeamData hergiswil = getTeamByName(teamData, "Hergiswil").orElseThrow();
         hergiswil.isPromotionDeclinedByClub = true;

         TeamData rothenburgIi = getTeamByName(teamData, "Rothenburg II").orElseThrow();
         rothenburgIi.isPromotionDeclinedByClub = true;

         TeamData buochs = getTeamByName(teamData, "Buochs").orElseThrow();
         buochs.isPromotionDeclinedByClub = true;


         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(baarIv.isPromotedRegularly, is(false));
         assertThat(baarIv.isPromotedAdditional, is(false));

         assertThat(kriensVi.isPromotedRegularly, is(false));
         assertThat(kriensVi.isPromotedAdditional, is(false));

         assertThat(sarnenIi.isPromotedRegularly, is(false));
         assertThat(sarnenIi.isPromotedAdditional, is(false));

         assertThat(hergiswil.isPromotedRegularly, is(false));
         assertThat(hergiswil.isPromotedAdditional, is(false));

         assertThat(rothenburgIi.isPromotedRegularly, is(false));
         assertThat(rothenburgIi.isPromotedAdditional, is(false));

         assertThat(buochs.isPromotedRegularly, is(false));
         assertThat(buochs.isPromotedAdditional, is(false));



         TeamData emmen = getTeamByName(teamData, "Emmen").orElseThrow();
         assertThat(emmen.isPromotedRegularly, is(false));
         assertThat(emmen.isPromotedAdditional, is(true));

         TeamData rothenburgIii = getTeamByName(teamData, "Rothenburg III").orElseThrow();
         assertThat(rothenburgIii.isPromotedRegularly, is(false));
         assertThat(rothenburgIii.isPromotedAdditional, is(true));
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Too many Teams of same Club get promoted")
   void tooManyTeamsOfClubInHigherLeague () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData goldau = getTeamByName(teamData, "Goldau").orElseThrow();
         goldau.isPromotionDeclinedByClub = true;

         TeamData rapidV = getTeamByName(teamData, "Rapid Luzern V").orElseThrow();
         rapidV.isPromotionDeclinedByClub = true;

         TeamData dierikon = getTeamByName(teamData, "Dierikon-Ebikon").orElseThrow();
         dierikon.isPromotionDeclinedByClub = true;

         TeamData baarIii = getTeamByName(teamData, "Baar III").orElseThrow();
         baarIii.isPromotionDeclinedByClub = true;

         TeamData rothenburg = getTeamByName(teamData, "Rothenburg").orElseThrow();
         rothenburg.isPromotionDeclinedByClub = true;

         TeamData zug = getTeamByName(teamData, "Zug").orElseThrow();
         zug.isPromotionDeclinedByClub = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(goldau.isPromotedRegularly, is(false));
         assertThat(goldau.isPromotedAdditional, is(false));

         assertThat(rapidV.isPromotedRegularly, is(false));
         assertThat(rapidV.isPromotedAdditional, is(false));

         assertThat(dierikon.isPromotedRegularly, is(false));
         assertThat(dierikon.isPromotedAdditional, is(false));

         assertThat(rothenburg.isPromotedRegularly, is(false));
         assertThat(rothenburg.isPromotedAdditional, is(false));

         assertThat(zug.isPromotedRegularly, is(false));
         assertThat(zug.isPromotedAdditional, is(false));


         TeamData kriensIv = getTeamByName(teamData, "Kriens IV").orElseThrow();
         assertThat(kriensIv.isPromotedRegularly, is(false));
         assertThat(kriensIv.isPromotedAdditional, is(false));

         TeamData rapidVi = getTeamByName(teamData, "Rapid Luzern VI").orElseThrow();
         assertThat(rapidVi.isPromotedRegularly, is(false));
         assertThat(rapidVi.isPromotedAdditional, is(true));
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Too many Teams of same Club get promoted second has higher LRI")
   void tooManyTeamsOfClubInHigherLeagueSecondHasHigherLri () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData goldau = getTeamByName(teamData, "Goldau").orElseThrow();
         goldau.isPromotionDeclinedByClub = true;

         TeamData rapidV = getTeamByName(teamData, "Rapid Luzern V").orElseThrow();
         rapidV.isPromotionDeclinedByClub = true;

         TeamData dierikon = getTeamByName(teamData, "Dierikon-Ebikon").orElseThrow();
         dierikon.isPromotionDeclinedByClub = true;

         TeamData baarIii = getTeamByName(teamData, "Baar III").orElseThrow();
         baarIii.isPromotionDeclinedByClub = true;

         TeamData rothenburg = getTeamByName(teamData, "Rothenburg").orElseThrow();
         rothenburg.isPromotionDeclinedByClub = true;

         TeamData zug = getTeamByName(teamData, "Zug").orElseThrow();
         zug.isPromotionDeclinedByClub = true;

         TeamData kriensIv = getTeamByName(teamData,"Kriens IV").orElseThrow();
         kriensIv.leagueRankingIndex = 31E9D;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(goldau.isPromotedRegularly, is(false));
         assertThat(goldau.isPromotedAdditional, is(false));

         assertThat(rapidV.isPromotedRegularly, is(false));
         assertThat(rapidV.isPromotedAdditional, is(false));

         assertThat(dierikon.isPromotedRegularly, is(false));
         assertThat(dierikon.isPromotedAdditional, is(false));

         assertThat(rothenburg.isPromotedRegularly, is(false));
         assertThat(rothenburg.isPromotedAdditional, is(false));

         assertThat(zug.isPromotedRegularly, is(false));
         assertThat(zug.isPromotedAdditional, is(false));


         assertThat(kriensIv.isPromotedRegularly, is(true));
         assertThat(kriensIv.isPromotedAdditional, is(false));

         TeamData kriensIii = getTeamByName(teamData, "Kriens III").orElseThrow();
         assertThat(kriensIii.isPromotedRegularly, is(false));
         assertThat(kriensIii.isPromotedAdditional, is(false));


         TeamData rapidVi = getTeamByName(teamData, "Rapid Luzern VI").orElseThrow();
         assertThat(rapidVi.isPromotedRegularly, is(true));
         assertThat(rapidVi.isPromotedAdditional, is(false));
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Voluntary Relegation causes additional Promotion in lower league")
   void voluntaryRelegationOfTeam () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData baar = getTeamByName(teamData, "Baar").orElseThrow();
         baar.isRelegatedByClub = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(baar.isRelegatedRegularly, is(false));
         assertThat(baar.isRelegatedAdditional, is(true));
         assertThat(baar.isAllowedForNationalLeaguePromotion, is(false));

         TeamData rapidVi = getTeamByName(teamData, "Rapid Luzern VI").orElseThrow();
         assertThat(rapidVi.isPromotedRegularly, is(false));
         assertThat(rapidVi.isPromotedAdditional, is(true));

         TeamData kriensIi = getTeamByName(teamData, "Kriens II").orElseThrow();
         assertThat(kriensIi.isPromotedRegularly, is(false));
         assertThat(kriensIi.isPromotedAdditional, is(false));
         assertThat(kriensIi.isAllowedForNationalLeaguePromotion, is(true));

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Voluntary Relegation ignored in lowest league")
   void voluntaryRelegationOfTeamInLowestLeague () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData rapidX = getTeamByName(teamData, "Rapid Luzern X").orElseThrow();
         rapidX.isRelegatedByClub = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(rapidX.isRelegatedRegularly, is(false));
         assertThat(rapidX.isRelegatedAdditional, is(false));
         assertThat(rapidX.isAllowedForNationalLeaguePromotion, is(false));
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Retreated teams get replaced")
   void retreatedTeamsGetReplaced () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData goldauIII = getTeamByName(teamData, "Goldau III").orElseThrow();
         goldauIII.isRetreatedByClub = true;

         TeamData goldauIv = getTeamByName(teamData, "Goldau IV").orElseThrow();
         goldauIv.isRetreatedByClub = true;

         TeamData emmenIii = getTeamByName(teamData, "Emmen III").orElseThrow();
         assertThat(emmenIii.isPromotedAdditional, is(false));

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(emmenIii.isPromotedRegularly, is(false));
         assertThat(emmenIii.isPromotedAdditional, is(true));

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Retreated teams don't get promoted")
   void retreatedTeamsDontGetPromoted () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData rothenburgVii = getTeamByName(teamData, "Rothenburg VII").orElseThrow();
         rothenburgVii.isPromotedRegularly = true;
         assertThat(rothenburgVii.isPromotedRegularly, is(true));
         rothenburgVii.isRetreatedByClub = true;

         TeamData dierikonEbikonV = getTeamByName(teamData, "Dierikon-Ebikon V").orElseThrow();
         assertThat(dierikonEbikonV.isPromotedRegularly, is(false));

         TeamData emmenIii = getTeamByName(teamData, "Emmen III").orElseThrow();
         assertThat(emmenIii.isPromotedAdditional, is(false));

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         assertThat(rothenburgVii.isPromotedRegularly, is(false));
         assertThat(rothenburgVii.isPromotedAdditional, is(false));

         assertThat(dierikonEbikonV.isPromotedRegularly, is(true));
         assertThat(dierikonEbikonV.isPromotedAdditional, is(false));

         assertThat(emmenIii.isPromotedRegularly, is(false));
         assertThat(emmenIii.isPromotedAdditional, is(true));

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }


   @Test
   @DisplayName("Teams promoted to NLC replaced")
   void teamsPromotedToNlcGetReplaced () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData schenkon = getTeamByName(teamData, "Schenkon").orElseThrow();
         schenkon.isPromotedToNLC = true;

         TeamData baar = getTeamByName(teamData, "Baar").orElseThrow();
         baar.isPromotedToNLC = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData rapidVi = getTeamByName(teamData, "Rapid Luzern VI").orElseThrow();
         assertThat(rapidVi.isPromotedAdditional, is(true));

         TeamData dierikon = getTeamByName(teamData, "Dierikon-Ebikon").orElseThrow();
         assertThat(dierikon.isPromotedAdditional, is(true));

         assertLeagueOneTeamCount(teamData, 8L);

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Teams promoted to NLC partially replaced with group size 9")
   void teamsPromotedToNlcGetPartiallyReplacedGS9 () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData schenkon = getTeamByName(teamData, "Schenkon").orElseThrow();
         schenkon.isPromotedToNLC = true;

         TeamData baar = getTeamByName(teamData, "Baar").orElseThrow();
         baar.isPromotedToNLC = true;

         addFillerTeam(teamData, null, 9);

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData rapidVi = getTeamByName(teamData, "Rapid Luzern VI").orElseThrow();
         assertThat(rapidVi.isPromotedAdditional, is(true));

         TeamData dierikon = getTeamByName(teamData, "Dierikon-Ebikon").orElseThrow();
         assertThat(dierikon.isPromotedAdditional, is(false));

         assertLeagueOneTeamCount(teamData, 8L);


      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Teams promoted to NLC partially replaced with group size 9")
   void teamsPromotedToNlcGetNotReplacedGS10 () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData schenkon = getTeamByName(teamData, "Schenkon").orElseThrow();
         schenkon.isPromotedToNLC = true;

         TeamData baar = getTeamByName(teamData, "Baar").orElseThrow();
         baar.isPromotedToNLC = true;

         addFillerTeam(teamData, null, 9);
         addFillerTeam(teamData, "II", 10);

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData rapidVi = getTeamByName(teamData, "Rapid Luzern VI").orElseThrow();
         assertThat(rapidVi.isPromotedAdditional, is(false));

         TeamData dierikon = getTeamByName(teamData, "Dierikon-Ebikon").orElseThrow();
         assertThat(dierikon.isPromotedAdditional, is(false));

         assertLeagueOneTeamCount(teamData, 8L);


      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }


   @Test
   @DisplayName("Team relegated from NLC expands group size fron 8 to 9")
   void teamRelegatedFromNlcDoesntPushTeamsThroughToSecondLeague () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         addRelegatedNlcTeam(teamData, "Rapid Luzern III");

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData rapidIv = getTeamByName(teamData, "Rapid Luzern IV").orElseThrow();
         assertThat(rapidIv.isRelegatedAdditional, is(false));

         assertLeagueOneTeamCount(teamData, 9L);


      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Two Team relegated from NLC expands group size fron 8 to 10")
   void twoTeamsRelegatedFromNlcDoesntPushTeamsThroughToSecondLeague () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         addRelegatedNlcTeam(teamData, "Rapid Luzern III");
         addRelegatedNlcTeam(teamData, "Kriens");

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData rapidIv = getTeamByName(teamData, "Rapid Luzern IV").orElseThrow();
         assertThat(rapidIv.isRelegatedAdditional, is(false));

         assertLeagueOneTeamCount(teamData, 10L);


      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Three teams relegated from NLC expands group size fron 8 to 10, one gets pushed down")
   void threeTeamsRelegatedFromNlcDoesntPushTeamsThroughToSecondLeague () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         addRelegatedNlcTeam(teamData, "Rapid Luzern III");
         addRelegatedNlcTeam(teamData, "Kriens");
         addRelegatedNlcTeam(teamData, "Dummy");

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData rapidIv = getTeamByName(teamData, "Rapid Luzern IV").orElseThrow();
         assertThat(rapidIv.isRelegatedAdditional, is(true));

         assertLeagueOneTeamCount(teamData, 10L);
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }


   @Test
   @DisplayName("Teams bound to be relegated from 1st League don't qualify for NLC promotion")
   void teamsRelegatedFromFirstLeagueDontPromoteToNlc () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData schenkon = getTeamByName(teamData, "Schenkon").orElseThrow();
         schenkon.isPromotionDeclinedByClub = true;
         TeamData baar = getTeamByName(teamData, "Baar").orElseThrow();
         baar.isPromotionDeclinedByClub = true;
         TeamData kriensIi = getTeamByName(teamData, "Kriens II").orElseThrow();
         kriensIi.isPromotionDeclinedByClub = true;
         TeamData huenenberg = getTeamByName(teamData, "Hünenberg").orElseThrow();
         huenenberg.isPromotionDeclinedByClub = true;
         TeamData reussbuehl = getTeamByName(teamData, "Reussbühl").orElseThrow();
         reussbuehl.isPromotionDeclinedByClub = true;
         TeamData rapidLuzernIv = getTeamByName(teamData, "Rapid Luzern IV").orElseThrow();
         rapidLuzernIv.isPromotionDeclinedByClub = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();

         TeamData sarnen = getTeamByName(teamData, "Sarnen").orElseThrow();
         TeamData steinhausen = getTeamByName(teamData, "Steinhausen").orElseThrow();

         assertThat(sarnen.isAllowedForNationalLeaguePromotion, is(false));
         assertThat(steinhausen.isAllowedForNationalLeaguePromotion, is(false));
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Test
   @DisplayName("Teams can be omitted from relegation if there are not enough teams from lower league to promote")
   void allowTeamToOmitRelegationIfNotEnoughPromotions () throws Exception {
      URI uri = Objects.requireNonNull(PromotionRelegationService.class.getResource(TEST_DATA_FILE)).toURI();
      InputStream inputStream = Files.newInputStream(Paths.get(uri));
      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, DataHandler.LIST_OF_TABLELINE_TYPE);

         TeamData knutwilIi = getTeamByName(teamData, "Knutwil II").orElseThrow();
         knutwilIi.isRetreatedByClub = true;
         TeamData emmenIii = getTeamByName(teamData, "Emmen III").orElseThrow();
         emmenIii.isPromotionDeclinedByClub = true;
         TeamData buochsIi = getTeamByName(teamData, "Buochs II").orElseThrow();
         buochsIi.isPromotionDeclinedByClub = true;
         TeamData rapidX = getTeamByName(teamData, "Rapid Luzern X").orElseThrow();
         rapidX.isPromotionDeclinedByClub = true;
         TeamData rapidIx = getTeamByName(teamData, "Rapid Luzern IX").orElseThrow();
         rapidIx.isPromotionDeclinedByClub = true;
         TeamData reussbuehlV = getTeamByName(teamData, "Reussbühl V").orElseThrow();
         reussbuehlV.isPromotionDeclinedByClub = true;
         TeamData goldauIv = getTeamByName(teamData, "Goldau IV").orElseThrow();
         goldauIv.isPromotionDeclinedByClub = true;
         TeamData schenkonV = getTeamByName(teamData, "Schenkon V").orElseThrow();
         schenkonV.isPromotionDeclinedByClub = true;
         TeamData sarnenIii = getTeamByName(teamData, "Sarnen III").orElseThrow();
         sarnenIii.isPromotionDeclinedByClub = true;

         TeamData rothenburgVi = getTeamByName(teamData, "Rothenburg VI").orElseThrow();
         rothenburgVi.isPromotionDeclinedByClub = true;

         TeamData rotkreuzII = getTeamByName(teamData, "Rotkreuz II").orElseThrow();
         rotkreuzII.isOmittanceOfRelegationAllowedByClub = true;

         service.setAllRecords(teamData);
         service.calculateStandardPromotionAndRelegation();


         assertThat(rotkreuzII.isRelegatedRegularly, is(false));
         assertThat(rotkreuzII.isRelegatedAdditional, is(false));
         assertThat(rotkreuzII.isRelegationOmittable, is(true));


      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }




   private static void assertLeagueOneTeamCount(List<TeamData> teamData, long value) {
      long count = teamData.stream()
            .filter(t -> t.series.equals(Series.HERREN))
            .filter(t -> (t.league == 1 && (!t.isPromotedToNLC && !t.isRelegatedRegularly && !t.isRelegatedAdditional))
                  || ((t.league == 2 && (t.isPromotedRegularly || t.isPromotedAdditional))))
            .count();
      assertThat(count, is(value));
   }


   private static void addFillerTeam(List<TeamData> teamData, String romanTeamNumber, int rank) {
      long groupId = teamData.stream()
            .filter(t -> Series.HERREN.equals(t.series))
            .filter(t -> t.league != rank)
            .filter(t -> t.clickTtRank > 0)
            .findFirst().orElseThrow().groupId;
      TeamData filler = new TeamData();
      filler.league = 1;
      filler.groupId = groupId;
      if (romanTeamNumber != null) {
         filler.teamName = "Filler " + romanTeamNumber;
      } else {
         filler.teamName = "Filler";
      }
      filler.clickTtRank = rank;
      filler.isInPromotionalLeague = true;
      filler.series = Series.HERREN;

      teamData.add(filler);
   }

   private static void addRelegatedNlcTeam(List<TeamData> teamData, String teamName) {
      long groupId = teamData.stream()
            .filter(t -> Series.HERREN.equals(t.series))
            .filter(t -> t.league == 1)
            .filter(t -> t.clickTtRank > 0)
            .findFirst().orElseThrow().groupId;
      TeamData relegatedTeam = new TeamData();
      relegatedTeam.league = 1;
      relegatedTeam.groupId = groupId;
      relegatedTeam.teamName = teamName;
      relegatedTeam.clickTtRank = 0;
      relegatedTeam.isInPromotionalLeague = true;
      relegatedTeam.isPromotionProhibited = true;
      relegatedTeam.leagueRankingIndex = 1E50D;
      relegatedTeam.groupRankingIndex = Long.MAX_VALUE;
      relegatedTeam.series = Series.HERREN;
      relegatedTeam.isRelegatedFromNLC = true;
      teamData.add(relegatedTeam);
   }

   private Optional<TeamData> getTeamByName(List<TeamData> allTeams, String teamName) {
      return allTeams.stream()
            .filter(t -> Series.HERREN.equals(t.series))
            .filter(t -> teamName.equals(t.teamName))
            .findFirst();
   }
}