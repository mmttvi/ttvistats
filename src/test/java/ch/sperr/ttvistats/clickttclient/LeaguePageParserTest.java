/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.clickttclient;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

/**
 * leaguePageParserTest
 */
class LeaguePageParserTest {

   @Test
   @DisplayName("LeaguePageParser extracts correct group id's")
   void parserExtractsCorrectGroupNumbers() throws Exception {

      URI uri = Objects.requireNonNull(LeaguePageParser.class.getResource("leaguepage.html")).toURI();
      String leaguePage = Files.readString(Paths.get(uri));

      Map<String, Long> groupMap = LeaguePageParser.extractGroups(leaguePage);

      assertThat(groupMap.size(), is(37));

      assertThat(groupMap.get("He 1. Liga Gr. 1"), is(208290L));
      assertThat(groupMap.get("He 2. Liga Gr. 1"), is(208291L));
      assertThat(groupMap.get("He 2. Liga Gr. 2"), is(208292L));
      assertThat(groupMap.get("He 3. Liga Gr. 4"), is(208286L));
      assertThat(groupMap.get("FSL Gr. 1"), is(208294L));
      assertThat(groupMap.get("O40-G2"), is(208725L));
      assertThat(groupMap.get("U13-R3-D1"), is(209364L));

   }

}