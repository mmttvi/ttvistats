/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.clickttclient;

import ch.sperr.ttvistats.data.TeamData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;


class GroupPageParserTest {

   @Test
   @DisplayName("GroupPageParser extracts correct date from lines without promotion flag")
   void parserExtractsCorrectData1() throws Exception {

      URI uri = Objects.requireNonNull(LeaguePageParser.class.getResource("grouppage.html")).toURI();
      String leaguePage = Files.readString(Paths.get(uri));

      List<TeamData> teamData = GroupPageParser.extractTable(leaguePage, 208929,"He 2.Liga Gr. 2", 4);

      assertThat(teamData.size(), is(8));
      TeamData teamData5 = teamData.get(4);
      assertThat(teamData5.groupId, is(208929L));
      assertThat(teamData5.groupName, is("He 2.Liga Gr. 2"));
      assertThat(teamData5.clickTtPromotionState, nullValue());
      assertThat(teamData5.teamName, is("Rotkreuz"));
      assertThat(teamData5.clickTtRank, is(5));
      assertThat(teamData5.nbrOfMatches, is(14));
      assertThat(teamData5.nbrOfMatchesWon, is(5));
      assertThat(teamData5.nbrOfMatchesTied, is(5));
      assertThat(teamData5.nbrOfMatchesLost, is(4));
      assertThat(teamData5.nbrOfGamesWon, is(72));
      assertThat(teamData5.nbrOfGamesLost, is(68));
      assertThat(teamData5.nbrOfSetsWon, is(258));
      assertThat(teamData5.nbrOfSetsLost, is(259));
      assertThat(teamData5.nbrOfBallsWon, is(4522));
      assertThat(teamData5.nbrOfBallsLost, is(4721));
      assertThat(teamData5.nbrOfPointsWon, is(29));
      assertThat(teamData5.nbrOfPointsLost, is(27));

   }

   @Test
   @DisplayName("GroupPageParser extracts correct date from lines with promotion flag PROMOTE")
   void parserExtractsCorrectData2() throws Exception {

      URI uri = Objects.requireNonNull(LeaguePageParser.class.getResource("grouppage.html")).toURI();
      String leaguePage = Files.readString(Paths.get(uri));

      List<TeamData> teamData = GroupPageParser.extractTable(leaguePage, 208929,"He 2.Liga Gr. 2", 4);

      assertThat(teamData.size(), is(8));
      TeamData teamData1 = teamData.get(0);
      assertThat(teamData1.groupId, is(208929L));
      assertThat(teamData1.groupName, is("He 2.Liga Gr. 2"));
      assertThat(teamData1.clickTtPromotionState, is(TeamData.PromotionState.PROMOTION));
      assertThat(teamData1.teamName, is("Kriens III"));
      assertThat(teamData1.clickTtRank, is(1));
      assertThat(teamData1.nbrOfMatches, is(14));
      assertThat(teamData1.nbrOfMatchesWon, is(10));
      assertThat(teamData1.nbrOfMatchesTied, is(2));
      assertThat(teamData1.nbrOfMatchesLost, is(2));
      assertThat(teamData1.nbrOfGamesWon, is(97));
      assertThat(teamData1.nbrOfGamesLost, is(43));
      assertThat(teamData1.nbrOfSetsWon, is(326));
      assertThat(teamData1.nbrOfSetsLost, is(198));
      assertThat(teamData1.nbrOfBallsWon, is(5126));
      assertThat(teamData1.nbrOfBallsLost, is(4394));
      assertThat(teamData1.nbrOfPointsWon, is(42));
      assertThat(teamData1.nbrOfPointsLost, is(14));

   }

   @Test
   @DisplayName("GroupPageParser extracts correct date from lines with promotion flag RELEGATION")
   void parserExtractsCorrectData3() throws Exception {

      URI uri = Objects.requireNonNull(LeaguePageParser.class.getResource("grouppage.html")).toURI();
      String leaguePage = Files.readString(Paths.get(uri));

      List<TeamData> teamData = GroupPageParser.extractTable(leaguePage, 208929,"He 2.Liga Gr. 2", 4);

      assertThat(teamData.size(), is(8));
      TeamData teamData7 = teamData.get(6);
      assertThat(teamData7.groupId, is(208929L));
      assertThat(teamData7.groupName, is("He 2.Liga Gr. 2"));
      assertThat(teamData7.clickTtPromotionState, is(TeamData.PromotionState.RELEGATION));
      assertThat(teamData7.teamName, is("Baar II"));
      assertThat(teamData7.clickTtRank, is(7));
      assertThat(teamData7.nbrOfMatches, is(14));
      assertThat(teamData7.nbrOfMatchesWon, is(4));
      assertThat(teamData7.nbrOfMatchesTied, is(1));
      assertThat(teamData7.nbrOfMatchesLost, is(9));
      assertThat(teamData7.nbrOfGamesWon, is(41));
      assertThat(teamData7.nbrOfGamesLost, is(99));
      assertThat(teamData7.nbrOfSetsWon, is(161));
      assertThat(teamData7.nbrOfSetsLost, is(326));
      assertThat(teamData7.nbrOfBallsWon, is(3079));
      assertThat(teamData7.nbrOfBallsLost, is(4796));
      assertThat(teamData7.nbrOfPointsWon, is(17));
      assertThat(teamData7.nbrOfPointsLost, is(39));

   }



}