/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.data;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.closeTo;

/**
 * TableLineTest
 */
@SuppressWarnings("EqualsWithItself")
class TeamDataTest {

   @ParameterizedTest(name = "{index}: {0} --> {1}")
   @DisplayName("Determine Club")
   @CsvSource({
         "Rapid Luzern, Rapid Luzern",
         "Rapid Luzern II, Rapid Luzern",
         "Rapid Luzern XX, Rapid Luzern",
         "Rapid X Luzern, Rapid X Luzern",
         "Rapid X Luzern XX, Rapid X Luzern",
         "'', unknown club",
         ", unknown club"
   })
   void determineClubTest(String teamName, String expectedClub) {
      TeamData teamData = new TeamData();
      teamData.teamName = teamName;

      assertThat(teamData.determineClub(), is(expectedClub));
   }


   @Test
   void calculateRankingIndices001() {
      TeamData teamData = new TeamData();
      teamData.nbrOfPointsWon = 10;
      teamData.nbrOfGamesWon = 70;
      teamData.nbrOfGamesLost = 30;
      teamData.nbrOfSetsWon = 250;
      teamData.nbrOfSetsLost = 125;
      teamData.nbrOfBallsWon = 2500;
      teamData.nbrOfBallsLost = 2000;
      teamData.nbrOfMatches = 10;

      teamData.calculateRankingIndices();

      assertThat(teamData.groupRankingIndex, is(100401250500L));
      assertThat(teamData.leagueRankingIndex, closeTo(10040125050.0D, 1E-14D));
   }

   @Test
   void calculateRankingIndices002() {
      TeamData teamData = new TeamData();
      teamData.nbrOfPointsWon = 0;
      teamData.nbrOfGamesWon = 10;
      teamData.nbrOfGamesLost = 90;
      teamData.nbrOfSetsWon = 70;
      teamData.nbrOfSetsLost = 270;
      teamData.nbrOfBallsWon = 2500;
      teamData.nbrOfBallsLost = 2700;
      teamData.nbrOfMatches = 10;

      teamData.calculateRankingIndices();

      assertThat(teamData.groupRankingIndex, is(-802000200L));
      assertThat(teamData.leagueRankingIndex, closeTo(-80200020.0D, 1E-14D));
   }

   @Test
   @DisplayName("Additional relegations sort before regular relegations")
   void omittableRelegationComparatorTest001() {
      TeamData teamDataA = new TeamData();
      teamDataA.isRelegatedRegularly = true;
      teamDataA.isRelegatedAdditional = false;
      teamDataA.leagueRankingIndex = 10.0D;

      TeamData teamDataB = new TeamData();
      teamDataB.isRelegatedRegularly = false;
      teamDataB.isRelegatedAdditional = true;
      teamDataB.leagueRankingIndex = 15.0D;

      List<TeamData> teamDatalist = new ArrayList<>();
      teamDatalist.add(teamDataA);
      teamDatalist.add(teamDataB);
      teamDatalist.sort(TeamData.getOmittableRelegationsComparator());

      assertThat(teamDatalist.get(0), is(teamDataB));

      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataA, teamDataB) > 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataB, teamDataA) < 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataA, teamDataA) == 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataB, teamDataB) == 0, is(true));
   }

   @Test
   @DisplayName("Regular relegations sort on leagueRankingIndex backwards")
   void omittableRelegationComparatorTest002() {
      TeamData teamDataA = new TeamData();
      teamDataA.isRelegatedRegularly = true;
      teamDataA.isRelegatedAdditional = false;
      teamDataA.leagueRankingIndex = 10.0D;

      TeamData teamDataB = new TeamData();
      teamDataB.isRelegatedRegularly = true;
      teamDataB.isRelegatedAdditional = false;
      teamDataB.leagueRankingIndex = 15.0D;

      List<TeamData> teamDatalist = new ArrayList<>();
      teamDatalist.add(teamDataA);
      teamDatalist.add(teamDataB);
      teamDatalist.sort(TeamData.getOmittableRelegationsComparator());

      assertThat(teamDatalist.get(0), is(teamDataB));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataA, teamDataB) > 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataB, teamDataA) < 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataA, teamDataA) == 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataB, teamDataB) == 0, is(true));
   }

   @Test
   @DisplayName("Additonal relegations sort on leagueRankingIndex backwards")
   void omittableRelegationComparatorTest003() {
      TeamData teamDataA = new TeamData();
      teamDataA.isRelegatedRegularly = false;
      teamDataA.isRelegatedAdditional = true;
      teamDataA.leagueRankingIndex = 10.0D;

      TeamData teamDataB = new TeamData();
      teamDataB.isRelegatedRegularly = false;
      teamDataB.isRelegatedAdditional = true;
      teamDataB.leagueRankingIndex = 15.0D;

      List<TeamData> teamDatalist = new ArrayList<>();
      teamDatalist.add(teamDataA);
      teamDatalist.add(teamDataB);
      teamDatalist.sort(TeamData.getOmittableRelegationsComparator());

      assertThat(teamDatalist.get(0), is(teamDataB));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataA, teamDataB) > 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataB, teamDataA) < 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataA, teamDataA) == 0, is(true));
      assertThat(TeamData.getOmittableRelegationsComparator().compare(teamDataB, teamDataB) == 0, is(true));
   }



}