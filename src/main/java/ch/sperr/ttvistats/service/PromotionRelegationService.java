/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import ch.sperr.ttvistats.data.TeamData;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * PromotionRelegationService
 */

@ApplicationScoped
public class PromotionRelegationService {


   @ConfigProperty(name = "ttvi.leagues.MaxGroupSize", defaultValue = "8")
   int maxGroupSize;
   @ConfigProperty(name = "ttvi.highest.league", defaultValue = "1")
   int highestLeague;
   @ConfigProperty(name = "ttvi.highest.league.maximum-group-size", defaultValue = "10")
   int highestLeagueMaxTeamSize;
   @ConfigProperty(name = "ttvi.highest.league.minimum-group-size", defaultValue = "8")
   int highestLeagueMinTeamSize;

   @ConfigProperty(name = "ttvi.lowest.league", defaultValue = "4")
   int lowestLeague;

   @ConfigProperty(name = "ttvi.maximum.allowed.teams-per-club-per-group", defaultValue = "2")
   int maxTeamsPerGroupAndClub;

   @ConfigProperty(name = "ttvi.teams-allowed-for-nlc-promotion", defaultValue = "2")
   int teamsAllowedForNlcPromotion;

   private boolean fixedTableLineSet = false;


   void setAllRecords(List<TeamData> allRecords) {
      resetRetreatedAndFilterIrrelevantRecords(allRecords.stream());
      this.fixedTableLineSet = true;
   }

   private List<TeamData> allRecords = null;

   @Transactional
   public void calculateStandardPromotionAndRelegation() {

      if (!fixedTableLineSet) {
         PanacheQuery<TeamData> allRecordsQuery = TeamData.findAll();
         resetRetreatedAndFilterIrrelevantRecords(allRecordsQuery.stream());
      }

      NavigableMap<Integer, LeagueCalculationResult> calculationResults = new TreeMap<>();
      NavigableMap<Integer,NavigableMap<Long, List<TeamData>>> leagueTree
            = buildLeagueTree(allRecords);
      leagueTree.remove(-1);

      TeamCountHelper teamCountHelper = new TeamCountHelper(allRecords, maxTeamsPerGroupAndClub);

      int promotionsToNationalLeague = Math.toIntExact(allRecords.stream()
            .filter(t -> t.isPromotedToNLC)
            .count());


      LeagueCalculationResult leagueCalculationResult = new LeagueCalculationResult(teamsAllowedForNlcPromotion, Collections.emptyList());
      calculationResults.put(0, leagueCalculationResult);

      int regularRelegationsNeeded;

      for (int league = highestLeague; league <= lowestLeague; league++) {
         regularRelegationsNeeded = league < lowestLeague ? leagueTree.get(league + 1).size() : 0;
         leagueCalculationResult = calculatePromotionsAndRelegationForLeague(
               leagueTree,
               teamCountHelper,
               league,
               leagueCalculationResult.maximumPromotions,
               regularRelegationsNeeded,
               promotionsToNationalLeague);
         calculationResults.put(league, leagueCalculationResult);
      }

      resetAllowanceForNlcPromotionForRelegatedTeamsInFirstLeague(leagueTree);
      checkForOmittableRelegations(teamCountHelper, calculationResults, promotionsToNationalLeague);
   }

   private void checkForOmittableRelegations(TeamCountHelper teamCountHelper,
                                             NavigableMap<Integer, LeagueCalculationResult> calculationResults,
                                             int promotionsToNationalLeague) {
      for (int league = highestLeague; league <= lowestLeague; league++) {
         long actualTeamCount = teamCountHelper.getLeagueTeamCount(league);
         long targetLeagueSize = calculateTargetLeagueSize(league, promotionsToNationalLeague, actualTeamCount);
         if (targetLeagueSize > actualTeamCount) {
            List<TeamData> teamsAllowedForOmittanceOfRelegation
                  = calculationResults.get(league - 1).teamsRelegationOmittanceAllowed;
            long maxOmittances = targetLeagueSize - actualTeamCount;
            final int fixedLeague = league;
            teamsAllowedForOmittanceOfRelegation.stream()
                  .filter(team -> teamCountHelper.isTeamOfClubAllowedInLeague(team.determineClub(), fixedLeague -1))
                  .sorted(TeamData.getOmittableRelegationsComparator())
                  .limit(maxOmittances)
                  .forEach(t -> {
                     t.isRelegatedAdditional = false;
                     t.isRelegatedRegularly = false;
                     t.isRelegationOmittable = true;
                     teamCountHelper.moveTeam(t.determineClub(), fixedLeague, fixedLeague - 1);
                  });
         }
      }
   }

   private void resetAllowanceForNlcPromotionForRelegatedTeamsInFirstLeague(NavigableMap<Integer,NavigableMap<Long, List<TeamData>>> leagueTree) {
      NavigableMap<Long, List<TeamData>> groupMap = leagueTree.get(1);
      groupMap.values().stream()
            .flatMap(List::stream)
            .filter(t -> t.isRelegatedRegularly)
            .forEach(t -> t.isAllowedForNationalLeaguePromotion = false);
   }

   private void resetRetreatedAndFilterIrrelevantRecords(Stream<TeamData> allRecordsQuery) {
      this.allRecords = allRecordsQuery
            .map(this::resetRetreated)
            .filter(t -> t.isInPromotionalLeague)
            .filter(t -> !t.isRetreatedByClub)
            .collect(Collectors.toList());
   }

   private TeamData resetRetreated(TeamData t) {
      if (t.isRetreatedByClub) {
         t.isPromotedToNLC = false;
         t.isPromotedRegularly = false;
         t.isPromotedAdditional = false;
         t.isRelegatedRegularly = false;
         t.isRelegatedAdditional = false;
         t.isRelegatedFromNLC = false;
      }
      return t;
   }

   private LeagueCalculationResult calculatePromotionsAndRelegationForLeague(
         NavigableMap<Integer, NavigableMap<Long, List<TeamData>>> leagueTree,
         TeamCountHelper teamCountHelper,
         int league,
         long maxPromotions,
         int regularRelegationsNeeded,
         int nationalLeaguePromotions) {

      NavigableMap<Long, List<TeamData>> groupMap = leagueTree.get(league);
      List<TeamData> regularPromotions = new ArrayList<>();
      List<TeamData> omittableRelegations = new ArrayList<>();

      long initialLeagueTeamCount = teamCountHelper.getLeagueTeamCount(league);
      long targetLeagueSize = calculateTargetLeagueSize(league, nationalLeaguePromotions, initialLeagueTeamCount);

      groupMap.values().stream().forEach(
            l -> l.sort((a, b) -> Long.compare(b.groupRankingIndex, a.groupRankingIndex)));

      resetPromotionCalculation(groupMap);

      calculateRegularPromotions(teamCountHelper, league, groupMap, regularPromotions);
      calculateExtraPromotions(teamCountHelper, maxPromotions, groupMap, regularPromotions);

      calculateRegularRelegations(teamCountHelper, regularRelegationsNeeded, groupMap, omittableRelegations);
      calculateVoluntaryRelegations(teamCountHelper, league, groupMap);
      calculateAdditionalRelegations(teamCountHelper,
            league,
            regularRelegationsNeeded,
            groupMap,
            targetLeagueSize,
            omittableRelegations);

      return new LeagueCalculationResult(targetLeagueSize - teamCountHelper.getLeagueTeamCount(league),
            omittableRelegations);
   }

   private static void calculateAdditionalRelegations(TeamCountHelper teamCountHelper,
                                                      int league,
                                                      int regularRelegationsNeeded,
                                                      NavigableMap<Long, List<TeamData>> groupMap,
                                                      long targetLeagueSize, List<TeamData> omittableRelegations) {
      Optional<String> compromisingClub = teamCountHelper.getFirstCompromisingTeamInLeague(league);
      while (compromisingClub.isPresent()) {
         String compromisingClubName = compromisingClub.get();
         TeamData compromisingTeam = groupMap.values().stream()
               .flatMap(List::stream)
               .filter(l -> l.determineClub().equals(compromisingClubName))
               .sorted(Comparator.comparingDouble(a -> a.leagueRankingIndex))
               .findFirst().orElseThrow(
                     () -> new TtviStatsException("Invalid state: no team of compromising club found"));
         compromisingTeam.isRelegatedAdditional = true;
         teamCountHelper.moveTeam(compromisingClubName, league, league + 1);
         compromisingClub = teamCountHelper.getFirstCompromisingTeamInLeague(league);
      }

      long actualTeamCount = teamCountHelper.getLeagueTeamCount(league);
      long targetLeagueSizeBeforePromotionsFromLowerLeague = targetLeagueSize - regularRelegationsNeeded;

      // additional relegations
      groupMap.values().stream()
            .flatMap(List::stream)
            .filter(l -> !l.isPromotedRegularly)
            .filter(l -> !l.isAllowedForNationalLeaguePromotion)
            .filter(l -> !l.isRelegatedRegularly)
            .sorted(Comparator.comparingDouble(a -> a.leagueRankingIndex))
            .limit(Long.max(0, actualTeamCount - targetLeagueSizeBeforePromotionsFromLowerLeague))
            .forEach(l -> {
               l.isRelegatedAdditional = true;
               teamCountHelper.moveTeam(l.determineClub(), league, league + 1);
               if (l.isOmittanceOfRelegationAllowedByClub) {
                  omittableRelegations.add(l);
               }
            });
   }

   private long calculateTargetLeagueSize(int league, int nationalLeaguePromotions, long initialLeagueTeamCount) {
      long targetLeagueSize;
      if (league == highestLeague) {
         targetLeagueSize =
               Long.max(highestLeagueMinTeamSize,
                     Long.min(highestLeagueMaxTeamSize,
                           initialLeagueTeamCount - nationalLeaguePromotions));
      } else {
         targetLeagueSize = maxGroupSize * Math.round(Math.pow(2.0D , league - 1.0D));
      }
      return targetLeagueSize;
   }

   private void calculateVoluntaryRelegations(TeamCountHelper teamCountHelper,
                                              int league,
                                              NavigableMap<Long, List<TeamData>> groupMap) {
      // voluntary relegations
      if (lowestLeague != league) {
         groupMap.values().stream()
               .flatMap(List::stream)
               .filter(t -> t.isRelegatedByClub)
               .filter(t -> !t.isRelegatedRegularly)
               .forEach(team -> {
                  team.isRelegatedAdditional = true;
                  teamCountHelper.moveTeam(team.determineClub(), team.league, team.league + 1);
               });
      }
   }

   private static void calculateRegularRelegations(TeamCountHelper teamCountHelper,
                                                   int regularRelegationsNeeded,
                                                   NavigableMap<Long, List<TeamData>> groupMap,
                                                   List<TeamData> omittableRelegations) {
      List<TeamData> regularRelegations = new ArrayList<>();
      int regularRelegationsToDo = regularRelegationsNeeded;
      int rankInGroup = -1;
      while (regularRelegationsToDo > 0) {

         int finalRankInGroup = rankInGroup;
         groupMap.values().stream()
               .map(l -> l.get(l.size() + finalRankInGroup))
               .sorted(TeamData.getAdditionalRelegationsComparator())
               .filter(teamData ->
                     !(teamData.isPromotedRegularly || teamData.isPromotedAdditional || teamData.isPromotedToNLC))
               .limit(regularRelegationsToDo)
               .map(l -> {
                  l.isRelegatedRegularly = true;
                  return l;
               })
               .forEach(regularRelegations::add);

         regularRelegationsToDo = regularRelegationsNeeded - regularRelegations.size();
         rankInGroup--;

      }

      regularRelegations.stream()
            .forEach(team -> teamCountHelper.moveTeam(team.determineClub(), team.league, team.league + 1));
      regularRelegations.stream()
            .filter(team -> team.isOmittanceOfRelegationAllowedByClub)
            .forEach(omittableRelegations::add);
   }

   private static void calculateExtraPromotions(TeamCountHelper teamCountHelper,
                                                long maxPromotions,
                                                NavigableMap<Long, List<TeamData>> groupMap,
                                                List<TeamData> regularPromotions) {
      groupMap.values().stream().flatMap(List::stream)
            .filter(l -> !l.isPromotedRegularly)
            .filter(l -> !l.isPromotionProhibited)
            .filter(l -> !l.isAllowedForNationalLeaguePromotion)
            .filter(l -> !l.isPromotionDeclinedByClub)
            .filter(l -> !l.isRelegatedByClub)
            .sorted(TeamData.getAdditionalPromotionsComparator())
            .filter(l -> teamCountHelper.isTeamOfClubAllowedInLeague(l.determineClub(), l.league -1))
            .limit(Long.max(0, maxPromotions - regularPromotions.size()))
            .forEach(l -> {
               l.isPromotedAdditional = true;
               teamCountHelper.moveTeam(l.determineClub(), l.league, l.league -1);
            });
   }

   private void calculateRegularPromotions(TeamCountHelper teamCountHelper,
                                           int league,
                                           NavigableMap<Long, List<TeamData>> groupMap,
                                           List<TeamData> regularPromotions) {
      Map<String, Integer> teamCountState = teamCountHelper.getState();
      List<TeamData> excessPromotions = new ArrayList<>();

      boolean isHighestLeague = league == highestLeague;

      do {
         resetCurrentPromotionCalculations(teamCountHelper, groupMap, regularPromotions, teamCountState, excessPromotions);

         int regularPromotionsPerGroup = isHighestLeague ? teamsAllowedForNlcPromotion : 1;
         int regularPromotionsNeeded = groupMap.size() * regularPromotionsPerGroup;
         int rank = 0;
         int rankLimit = groupMap.values().stream()
               .map(List::size)
               .max(Integer::compareTo).orElseThrow();
         Set<Long> groupsDone = new HashSet<>();

         calculateRegularPromotionsIgnoringTeamCountInHigherLeague(teamCountHelper,
               league,
               groupMap,
               regularPromotions,
               excessPromotions,
               isHighestLeague,
               regularPromotionsNeeded,
               rank,
               rankLimit,
               groupsDone);

         markExcessPromotions(regularPromotions, excessPromotions);
      } while (!excessPromotions.isEmpty());
   }

   private static void markExcessPromotions(List<TeamData> regularPromotions, List<TeamData> excessPromotions) {
      excessPromotions.stream()
            .forEach(teamOnTest -> {
               Optional<TeamData> first = regularPromotions.stream()
                     .filter(team -> team.determineClub().equals(teamOnTest.determineClub()))
                     .sorted(Comparator.comparingDouble(a -> a.leagueRankingIndex))
                     .findFirst();
               if (first.isPresent() && first.get().leagueRankingIndex < teamOnTest.leagueRankingIndex) {
                  first.get().isPromotionProhibited = true;
               } else {
                  teamOnTest.isPromotionProhibited = true;
               }
            });
   }

   private static void calculateRegularPromotionsIgnoringTeamCountInHigherLeague(TeamCountHelper teamCountHelper,
                                                                                 int league,
                                                                                 NavigableMap<Long, List<TeamData>> groupMap,
                                                                                 List<TeamData> regularPromotions,
                                                                                 List<TeamData> excessPromotions,
                                                                                 boolean isHighestLeague,
                                                                                 int regularPromotionsNeeded,
                                                                                 int rank,
                                                                                 int rankLimit,
                                                                                 Set<Long> groupsDone) {
      while (rank < rankLimit
            && regularPromotions.size() < regularPromotionsNeeded) {
         int finalRank = rank;
         groupMap.values().stream()
               .filter(l -> l.size() > finalRank)
               .map(l -> l.get(finalRank))
               .filter(r -> isHighestLeague || !groupsDone.contains(r.groupId))
               .filter(team -> !team.isPromotionProhibited)
               .filter(team -> !team.isRelegatedByClub)
               .filter(team -> !team.isPromotionDeclinedByClub)
               .filter(team -> !team.isRelegatedFromNLC)
               .forEach(team -> {
                  if (regularPromotions.size() < regularPromotionsNeeded) {
                     if (teamCountHelper.isTeamOfClubAllowedInLeague(team.determineClub(), league - 1)) {
                        groupsDone.add(team.groupId);
                        regularPromotions.add(team);
                        if (isHighestLeague && !team.isPromotedToNLC) {
                           team.isAllowedForNationalLeaguePromotion = true;
                        } else {
                           teamCountHelper.moveTeam(team.determineClub(), league, league - 1);
                           team.isPromotedRegularly = true;
                        }
                     } else {
                        excessPromotions.add(team);
                     }
                  }
               });
         rank++;
      }
   }

   private static void resetCurrentPromotionCalculations(TeamCountHelper teamCountHelper,
                                                         NavigableMap<Long, List<TeamData>> groupMap,
                                                         List<TeamData> regularPromotions,
                                                         Map<String, Integer> teamCountState,
                                                         List<TeamData> excessPromotions) {
      teamCountHelper.resetState(teamCountState);
      regularPromotions.clear();
      excessPromotions.clear();
      groupMap.values().stream()
            .flatMap(List::stream)
            .forEach(l -> {
               l.isPromotedRegularly = false;
               l.isPromotedAdditional = false;
               l.isAllowedForNationalLeaguePromotion = false;});
   }

   private static void resetPromotionCalculation(NavigableMap<Long, List<TeamData>> groupMap) {
      groupMap.values().stream()
            .flatMap(List::stream)
            .forEach(l -> {
               l.isPromotionProhibited = false;
               l.isPromotedRegularly = false;
               l.isPromotedAdditional = false;
               l.isRelegatedRegularly = false;
               l.isRelegatedAdditional = false;
               l.isAllowedForNationalLeaguePromotion = false;});
   }


   private NavigableMap<Integer, NavigableMap<Long, List<TeamData>>> buildLeagueTree(List<TeamData> allRecords) {
      NavigableMap<Integer,NavigableMap<Long, List<TeamData>>> leagueTree = new TreeMap<>();
      allRecords.stream().forEach(row -> {
         NavigableMap<Long, List<TeamData>> groupMap = leagueTree.getOrDefault(row.league, new TreeMap<>());
         if (groupMap.isEmpty()) {
            leagueTree.put(row.league, groupMap);
         }
         List<TeamData> teamList = groupMap.getOrDefault(row.groupId, new ArrayList<>());
         if (teamList.isEmpty()) {
            groupMap.put(row.groupId, teamList);
         }
         teamList.add(row);
      });

      return leagueTree;
   }


   private static class LeagueCalculationResult {
      long maximumPromotions;
      List<TeamData> teamsRelegationOmittanceAllowed;

      LeagueCalculationResult(long maximumPromotions, List<TeamData> teamsRelegationOmittanceAllowed) {
         this.maximumPromotions = maximumPromotions;
         this.teamsRelegationOmittanceAllowed = teamsRelegationOmittanceAllowed;
      }
   }

}
