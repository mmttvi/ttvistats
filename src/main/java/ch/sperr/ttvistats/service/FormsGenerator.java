/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import ch.sperr.ttvistats.data.Series;
import ch.sperr.ttvistats.data.TeamData;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.alignment.HorizontalAlignment;
import com.lowagie.text.alignment.VerticalAlignment;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.vaadin.quarkus.annotation.UIScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.librepdf.openpdf.fonts.Liberation;

import javax.inject.Inject;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * FormsGenerator
 */
@UIScoped
public class FormsGenerator {

   private final Font baseFont;
   private final Font paddingFont;
   private final Font smallFont;
   private final Image logo;
   private final Font largeFont;
   private final Font footnoteFont;
   private final Font boldFont;
   private final Font smallBoldFont;





   @ConfigProperty(name = "ttvi.forms.returnAddress")
   String returnAddress;

   @ConfigProperty(name = "ttvi.formset1.isDraft", defaultValue = "true")
   boolean formSetOneIsDraft;

   @ConfigProperty(name = "ttvi.formset2.isDraft", defaultValue = "true")
   boolean formSetTwoIsDraft;

   @ConfigProperty(name = "ttvi.lowest.league", defaultValue = "4")
   int lowestLeague;

   @Inject
   DataHandler dataHandler;
   private static final Color LIGHT_GRAY = new Color(240, 240, 240);
   private static final Color GRAY = new Color(220, 220, 220);

   public FormsGenerator() {

      try {
         baseFont = Liberation.SANS.create(10);
         smallFont = Liberation.SANS.create(8);
         largeFont = Liberation.SANS.create(14);
         footnoteFont = Liberation.SANS.create(6);
         boldFont = Liberation.SANS.create(10);
         boldFont.setStyle(Font.BOLD);
         smallBoldFont = Liberation.SANS_BOLD.create(8);

         paddingFont = Liberation.SANS.create(1);

         URL ttviLogoUrl = FormsGenerator.class.getResource("images/ttvi_logo.png");

         assert ttviLogoUrl != null;
         logo = Image.getInstance(ttviLogoUrl);
         logo.scalePercent(5.0f);
         logo.setAlignment(Element.ALIGN_RIGHT);

      } catch (IOException e) {
         throw new TtviStatsException(e);
      }
   }


   public InputStream generateFirstFormSet() {
      return new ByteArrayInputStream(getFirstFormSet());
   }

   public InputStream generateFirstFormSetAsZip() {
      return new ByteArrayInputStream(getFirstFormSetAsZip());
   }

   public InputStream generateSecondFormSet() {
      return new ByteArrayInputStream(getSecondFormSet());
   }

   public InputStream generateSecondFormSetAsZip() {
      return new ByteArrayInputStream(getSecondFormSetAsZip());
   }


   private byte[] getFirstFormSet() {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

      Map<String, List<TeamData>> clubTeamMap = dataHandler.getDataAsList().stream()
            .filter(t -> t.isInPromotionalLeague)
            .sorted(TeamData.getStandardComparator())
            .collect(Collectors.groupingBy(TeamData::determineClub));

      try {
         Document document = initializeDocument(
               outputStream,
               formSetOneIsDraft,
               "TTVI - Mannschaftsrückzüge etc",
               "Formular zum Melden von Mannschaftsrückzügen etc.");

         clubTeamMap.entrySet().stream()
               .sorted(Map.Entry.comparingByKey())
               .forEach(e -> addFirstFormForClub(e.getKey(), e.getValue(), document));

         document.close();

      } catch (DocumentException exception) {
         System.err.println(Arrays.toString(exception.getStackTrace()));
      }


      return outputStream.toByteArray();
   }

   private byte[] getFirstFormSetAsZip() {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      try {
         ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
         zipOutputStream.setLevel(9);

         Map<String, List<TeamData>> clubTeamMap = dataHandler.getDataAsList().stream()
               .filter(t -> t.isInPromotionalLeague)
               .sorted(TeamData.getStandardComparator())
               .collect(Collectors.groupingBy(TeamData::determineClub));

         clubTeamMap.entrySet().stream()
               .sorted(Map.Entry.comparingByKey())
               .forEach(e -> {
                  try {
                     ByteArrayOutputStream docStream = new ByteArrayOutputStream();
                     Document document = initializeDocument(
                           docStream,
                           formSetOneIsDraft,
                           "TTVI - Mannschaftsrückzüge etc",
                           "Formular zum Melden von Mannschaftsrückzügen etc.");

                     addFirstFormForClub(e.getKey(), e.getValue(), document);
                     document.close();
                     ByteArrayInputStream docInputStream = new ByteArrayInputStream(docStream.toByteArray());
                     ZipEntry zipEntry = new ZipEntry("RZ-" + e.getKey() + ".pdf");
                     zipOutputStream.putNextEntry(zipEntry);
                     docInputStream.transferTo(zipOutputStream);
                     zipOutputStream.closeEntry();
                  } catch (DocumentException | IOException exception) {
                     System.err.println(Arrays.toString(exception.getStackTrace()));
                  }
               });
         zipOutputStream.close();
      } catch (IOException ex) {
         System.err.println(Arrays.toString(ex.getStackTrace()));
      }
      return outputStream.toByteArray();
   }

   private byte[] getSecondFormSet() {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

      Map<String, List<TeamData>> clubTeamMap = dataHandler.getDataAsList().stream()
            .sorted(TeamData.getStandardComparator())
            .collect(Collectors.groupingBy(TeamData::determineClub));

      try {
         Document document = initializeDocument(
               outputStream,
               formSetTwoIsDraft,
               "TTVI - Mannschaftsmeldung etc",
               "Formulare zur Mannschaftsmeldung etc.");

         clubTeamMap.entrySet().stream()
               .sorted(Map.Entry.comparingByKey())
               .forEach(e -> addSecondFormForClub(e.getKey(), e.getValue(), document));

         document.close();
      } catch (DocumentException exception) {
         System.err.println(Arrays.toString(exception.getStackTrace()));
      }


      return outputStream.toByteArray();
   }

   private byte[] getSecondFormSetAsZip() {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

      try {

         ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
         zipOutputStream.setLevel(9);

         Map<String, List<TeamData>> clubTeamMap = dataHandler.getDataAsList().stream()
               .sorted(TeamData.getStandardComparator())
               .collect(Collectors.groupingBy(TeamData::determineClub));


         clubTeamMap.entrySet().stream()
               .sorted(Map.Entry.comparingByKey())
               .forEach(e -> {
                  try {
                     ByteArrayOutputStream docStream = new ByteArrayOutputStream();
                     Document document = initializeDocument(
                           docStream,
                           formSetTwoIsDraft,
                           "TTVI - Mannschaftsmeldung etc",
                           "Formulare zur Mannschaftsmeldung etc.");
                     addSecondFormForClub(e.getKey(), e.getValue(), document);
                     document.close();
                     ByteArrayInputStream docInputStream = new ByteArrayInputStream(docStream.toByteArray());
                     ZipEntry zipEntry = new ZipEntry("MM-" + e.getKey() + ".pdf");
                     zipOutputStream.putNextEntry(zipEntry);
                     docInputStream.transferTo(zipOutputStream);
                     zipOutputStream.closeEntry();
                  } catch (DocumentException | IOException exception) {
                     System.err.println(Arrays.toString(exception.getStackTrace()));
                  }
               });
         zipOutputStream.close();
      } catch (IOException exception) {
         System.err.println(Arrays.toString(exception.getStackTrace()));
      }
      return outputStream.toByteArray();
   }

   private Document initializeDocument(OutputStream outputStream, boolean isDraft, String title, String subject) {
      Document document = new Document();
      PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);

      if (isDraft) {
         pdfWriter.setPageEvent(new PageWaterMark("ENTWURF"));
      }
      document.open();
      document.addTitle(title);
      document.addSubject(subject);
      document.addCreator("TTVI - Saisonabschluss Helfer");
      return document;
   }



   private void addFirstFormForClub(String clubName, List<TeamData> teams, Document document) {

      document.add(logo);
      document.add(buildParagraph("FO04150 / V01\n\n", baseFont));

      document.add(buildParagraph("Rückzüge/Verzichte auf Aufstieg/Freiwillige Abstiege MM Elite\n\n",
            largeFont));
      document.add(new LineSeparator());


      document.add(buildParagraph("Klub: " + clubName, largeFont));

      Table teamTable = new Table(8);
      teamTable.getDefaultCell().setVerticalAlignment(VerticalAlignment.CENTER);
      teamTable.getDefaultCell().setUseBorderPadding(true);
      teamTable.setHorizontalAlignment(HorizontalAlignment.LEFT);
      teamTable.setWidth(100);
      teamTable.setWidths(new int[]{10, 2, 5, 4, 4, 4, 4, 4});
      teamTable.setLastHeaderRow(2);
      Cell teamCell = buildStandardCell("Mannschaft", null, false, Color.WHITE);
      teamCell.setRowspan(2);
      teamTable.addCell(teamCell);
      Cell leagueCell = buildStandardCell("Liga", null, false, Color.WHITE);
      leagueCell.setRowspan(2);
      teamTable.addCell(leagueCell);
      Cell currentStateCell = buildStandardCell("aktueller Status", "1", false, Color.WHITE);
      currentStateCell.setRowspan(2);
      teamTable.addCell(currentStateCell);
      Cell cell = buildStandardCell("Obligatorische Angaben", "2", false, Color.WHITE);
      cell.setVerticalAlignment(VerticalAlignment.CENTER);
      cell.setColspan(3);
      teamTable.addCell(cell);
      cell = buildStandardCell("optionale Angaben", "3", false, Color.WHITE);
      cell.setColspan(2);
      teamTable.addCell(cell);
      cell = buildStandardCell("Verzicht Aufstieg", null, false, Color.WHITE);
      teamTable.addCell(cell);
      cell = buildStandardCell("freiwilliger Abstieg", null, false, Color.WHITE);
      teamTable.addCell(cell);
      cell = buildStandardCell("Rückzug", "4", false, Color.WHITE);
      teamTable.addCell(cell);
      cell = buildStandardCell("Aufstieg erwünscht", "5", false, Color.WHITE);
      teamTable.addCell(cell);
      cell = buildStandardCell("Verzicht Abstieg", "6", false, Color.WHITE);
      teamTable.addCell(cell);

      teams.stream()
            .sorted(TeamData.getStandardComparator())
            .forEach(t -> {
               teamTable.addCell(buildStandardCell(t.teamName, null, false, Color.WHITE));
               teamTable.addCell(buildStandardCell(Integer.toString(t.league), null, false, Color.WHITE));
               teamTable.addCell(buildStandardCell(getCurrentTeamStatus(t), null, false, Color.WHITE));

               for (int i = 0; i < 5; i++) {
                  teamTable.addCell(buildStandardCell("□ ja \n□ nein", null, false, Color.WHITE));
               }
            });
      document.add(teamTable);

      document.add(buildNote("1)", " Dieser Status basiert auf dem aktuellen Wissen und kann sich noch ändern."));
      document.add(buildNote("2)", " Diese Angaben müssen zwingend bis zum 30. April gemacht werden. " +
            "Ansonsten wird überall eine Nein angenommen. Bitte für alle Mannschaften ausfüllen. Es wird nicht mehr nachgefragt, " +
            "wenn sich der aktuelle Status einer Mannschaft ändert. D.h. z. Bsp. wenn eine Mannschaft auf den Aufstieg verzichten würde " +
            "falls sie doch noch aufsteigen könnte, muss das hier entsprechend jetzt schon angegeben werden."));
      document.add(buildNote("3)", " Diese Angaben können auch noch mit der Mannschaftsmeldung gemacht werden."));
      document.add(buildNote("4)", " Eine Mannschaft kann nur zurückgezogen werden, wenn alle " +
            "Mannschaften in tieferen Ligen auch zurückgezogen werden."));
      document.add(buildNote("5)", " Ein Ja bedeutet, dass die Mannschaft bereit wäre, auch eine Liga höher zu spielen."));
      document.add(buildNote("6)", " Ein Ja bedeutet, dass die Mannschaft bereit wäre, in der aktuellen Liga zu verbleiben."));


      buildSignatureTable(document);
      document.add(buildParagraph("Retour bis: 30. April", smallFont));

      document.newPage();
   }


   private void addSecondFormForClub(String clubName, List<TeamData> teams, Document document) {

      document.add(logo);
      document.add(buildParagraph("FO04111 / V03\n\n", baseFont));

      document.add(buildParagraph("Mannschaftsmeldungen MM Elite/Senioren\n\n",
            largeFont));
      document.add(new LineSeparator());


      document.add(buildParagraph("Klub: " + clubName, largeFont));

      Table layoutTable = buildLayoutTable();
      Table leftColumnTable = buildColumnTable(HorizontalAlignment.LEFT);
      Table rightColumnTable = buildColumnTable(HorizontalAlignment.RIGHT);


      for (Series series: Series.values()) {
         if (series.equals(Series.UNKNOWN)) {
            continue;
         }
         Table currentTable = rightColumnTable;

         if (series.equals(Series.HERREN) || series.equals(Series.FSL)) {
            currentTable = leftColumnTable;
         }
         currentTable.addCell(buildTitleCell(series));

         Table teamTable = buildTeamTable();

         teams.stream()
               .filter(t -> series.equals(t.series))
               .sorted(TeamData.getNextYearsLeagueComparator())
               .forEach(t -> buildTeamLine(teamTable, t));

         buildNewTeamsLine(series, teamTable);
         currentTable.insertTable(teamTable);
      }
      layoutTable.insertTable(leftColumnTable);
      layoutTable.addCell("");
      layoutTable.insertTable(rightColumnTable);
      document.add(layoutTable);

      addNotes(document);

      buildSignatureTable(document);
      document.add(buildParagraph("Retour bis: 31. Mai", smallFont));

      document.newPage();
   }

   private void addNotes(Document document) {
      document.add(buildNote("1)", " Die Mannschaftsnummern sind noch die aus letzter Saison. Sie werden anfangs Saison neu zugeordnet."));
      document.add(buildNote("2)", " Mannschaften ohne Angabe werden in die nächste Saison übernommen (nicht abgemeldet)."));
      document.add(buildNote("3)", " Hier die Anzahl neu zu meldender Mannschaften angeben."));
      document.add(buildNote("4)", " Hier die Anzahl neuer Mannschaften angeben, die bereit sind in der nächst höheren Liga zu spielen."));
   }

   private void buildNewTeamsLine(Series series, Table teamTable) {
      teamTable.addCell(buildStandardCell("Neue Mannschaften", null, false, LIGHT_GRAY));
      if (Series.HERREN.equals(series)) {
         teamTable.addCell(buildStandardCell(Integer.toString(lowestLeague), null, false, LIGHT_GRAY));
      } else {
         teamTable.addCell(buildStandardCell("1", null, false, LIGHT_GRAY));
      }
      teamTable.addCell(buildTitledCell("Anzahl:", " ", "3", Color.WHITE));
      if (Series.HERREN.equals(series)) {
         teamTable.addCell(buildTitledCell("Anzahl:", " ", "4", Color.WHITE));
      } else {
         teamTable.addCell(buildStandardCell("", null, false, LIGHT_GRAY));
      }
   }

   private void buildTeamLine(Table teamTable, TeamData t) {
      // Team Column
      teamTable.addCell(buildStandardCell(t.teamName, null, false, LIGHT_GRAY));
      // League Column
      int league = t.league
            + ((t.isPromotedRegularly || t.isPromotedAdditional) ? -1 : 0)
            + ((t.isRelegatedRegularly || t.isRelegatedAdditional) ? 1 : 0);
      if (t.isPromotedToNLC) {
         teamTable.addCell(buildStandardCell("NLC", null, false, LIGHT_GRAY));
      } else {
         teamTable.addCell(buildStandardCell(Integer.toString(league), null, false, LIGHT_GRAY));
      }

      // Opt-Out Column
      boolean canBeOptedOut = (t.isInRegisterLeague && !(t.isPromotedToNLC || t.isPromotedRegularly || t.isPromotedAdditional))
            || (league == lowestLeague);

      if (t.isRetreatedByClub) {
         teamTable.addCell(buildStandardCell("Rückzug", null, false, LIGHT_GRAY));
      } else if (canBeOptedOut) {
         teamTable.addCell(buildTitledCell("Abmeldung:", "□ ja □ nein", null, Color.WHITE));
      } else {
         teamTable.addCell(buildTitledCell("Abmeldung:","nicht möglich", null, LIGHT_GRAY));
      }

      // Next higher league ok column
      if (!Series.HERREN.equals(t.series) || ((league == 1 || league == 0) || t.isRetreatedByClub)) {
         teamTable.addCell(buildStandardCell("", null, false, LIGHT_GRAY));
      } else {
         teamTable.addCell(buildStandardCell("□ ja □ nein", null, false, Color.WHITE));
      }
   }

   private Table buildTeamTable() {
      Table teamTable = new Table(4);
      teamTable.getDefaultCell().setVerticalAlignment(VerticalAlignment.TOP);
      teamTable.getDefaultCell().setUseBorderPadding(true);
      teamTable.setHorizontalAlignment(HorizontalAlignment.LEFT);
      teamTable.setWidth(100);
      teamTable.setWidths(new int[]{10, 3, 9, 9});
      teamTable.setLastHeaderRow(1);
      Cell teamCell = buildStandardCell("Mannschaft", "1", true, GRAY);
      teamTable.addCell(teamCell);
      Cell cell = buildStandardCell("Liga", null, true, GRAY);
      teamTable.addCell(cell);
      cell = buildStandardCell("An-/Abmelden", "2", true, GRAY);
      teamTable.addCell(cell);
      cell = buildStandardCell("nächst höhere Liga i.O.", "3", true, GRAY);
      teamTable.addCell(cell);
      return teamTable;
   }

   private Cell buildTitleCell(Series series) {
      Chunk seriesChunk = new Chunk("\n" + series.getDescription(), boldFont);
      Cell titlecell = buildCellFromChunks(seriesChunk);
      titlecell.setHorizontalAlignment(HorizontalAlignment.LEFT);
      return titlecell;
   }

   private static Table buildColumnTable(HorizontalAlignment left) {
      Table leftColumnTable = new Table(1);
      leftColumnTable.setWidth(98);
      leftColumnTable.setBorder(0);
      leftColumnTable.getDefaultCell().setVerticalAlignment(VerticalAlignment.TOP);
      leftColumnTable.getDefaultCell().setHorizontalAlignment(HorizontalAlignment.LEFT);
      leftColumnTable.getDefaultCell().setBorder(0);
      leftColumnTable.setHorizontalAlignment(left);
      return leftColumnTable;
   }

   private static Table buildLayoutTable() {
      Table layoutTable = new Table(3, 1);
      layoutTable.setWidth(100);
      layoutTable.setBorder(0);
      layoutTable.getDefaultCell().setVerticalAlignment(VerticalAlignment.TOP);
      layoutTable.getDefaultCell().setBorder(0);
      layoutTable.setHorizontalAlignment(HorizontalAlignment.LEFT);
      layoutTable.setWidths(new int[]{100, 5, 100});
      layoutTable.setLastHeaderRow(0);
      return layoutTable;
   }

   private void buildSignatureTable(Document document) {
      Cell cell;
      Table signatureTable = new Table(2);
      signatureTable.setBorder(0);
      signatureTable.getDefaultCell().setVerticalAlignment(VerticalAlignment.CENTER);
      signatureTable.getDefaultCell().setBorder(0);
      signatureTable.setHorizontalAlignment(HorizontalAlignment.LEFT);
      signatureTable.setWidth(100);
      signatureTable.setWidths(new int[]{1, 4});
      signatureTable.setLastHeaderRow(0);
      cell = new Cell("Datum:");
      signatureTable.addCell(cell);
      cell = new Cell("Unterschrift:");
      signatureTable.addCell(cell);
      document.add(signatureTable);
      document.add(buildParagraph("\n\n\n\n" + returnAddress, smallFont));
   }


   private Paragraph buildParagraph(String content, Font font) {
      Chunk chunk = new Chunk(content, font);
      Paragraph paragraph = new Paragraph(chunk);
      paragraph.setAlignment(Element.ALIGN_LEFT);
      paragraph.setLeading(font.getSize() + 3);
      return paragraph;
   }

   private String getCurrentTeamStatus(TeamData t) {
      String result;

      if (t.isPromotedRegularly) {
         result = "regulärer Aufstieg";
      } else if (t.isRelegatedRegularly) {
         result = "regulärer Abstieg";
      } else if (t.isPromotedAdditional) {
         result = "zusätzlicher Aufstieg";
      } else if (t.isRelegatedAdditional) {
         result = "zusätzlicher Abstieg";
      } else {
         result = "Verbleib in Liga";
      }
      return result;
   }

   private Cell buildStandardCell(String text, String noteMarker, boolean bold, Color backgroundColor) {
      Chunk chunk = new Chunk(text, baseFont);
      if (bold) {
         chunk.setFont(boldFont);
      }

      Paragraph paragraph = new Paragraph();
      paragraph.add(chunk);
      if (noteMarker != null) {
         Chunk markerChunk = new Chunk(noteMarker, footnoteFont);
         markerChunk.setTextRise(6.0F);
         paragraph.add(markerChunk);
      }
      return buildFormattedCell(backgroundColor, paragraph);
   }

   private Cell buildTitledCell(String title, String text, String noteMarker, Color backgroundColor) {
      Chunk titleChunk = new Chunk(title, smallBoldFont);
      Chunk textChunk = new Chunk("\n" + text, smallFont);

      Paragraph paragraph = new Paragraph();
      paragraph.add(titleChunk);
      if (noteMarker != null) {
         Chunk markerChunk = new Chunk(noteMarker, footnoteFont);
         markerChunk.setTextRise(4.0F);
         paragraph.add(markerChunk);
      }
      paragraph.add(textChunk);
      return buildFormattedCell(backgroundColor, paragraph);
   }
   private Paragraph buildPaddingParagraph(float leading, float indentation) {
      Chunk paddingChunk = new Chunk("\n", paddingFont);
      Paragraph paddingParagraph = new Paragraph();
      paddingParagraph.add(paddingChunk);
      paddingParagraph.setLeading(leading, 0F);
      paddingParagraph.setIndentationLeft(indentation);
      return paddingParagraph;
   }

   private Cell buildFormattedCell(Color backgroundColor, Paragraph paragraph) {
      paragraph.setIndentationLeft(3.0F);
      paragraph.setLeading(12F, 1.1F);
      Cell cell = new Cell(buildPaddingParagraph(13.0F, 3.0F));
      cell.add(paragraph);
      cell.add(buildPaddingParagraph(4.0F, 0.0F));
      cell.setVerticalAlignment(VerticalAlignment.CENTER);
      cell.setUseAscender(true);
      cell.setUseDescender(true);
      cell.setBackgroundColor(backgroundColor);
      return cell;
   }


   private Cell buildCellFromChunks(Chunk... chunks) {
      Chunk paddingChunkTop = new Chunk("\n", paddingFont);
      Chunk paddingChunkBottom = new Chunk("\n\n", paddingFont);
      Paragraph paragraph = new Paragraph();
      paragraph.add(paddingChunkTop);
      paragraph.addAll(Arrays.asList(chunks));
      paragraph.add(paddingChunkBottom);
      paragraph.setIndentationLeft(0.0F);
      paragraph.setLeading(12F, 1.10F);
      Cell cell = new Cell(paragraph);
      cell.setVerticalAlignment(VerticalAlignment.TOP);
      cell.setUseAscender(true);
      cell.setUseDescender(true);
      return cell;
   }

   private Paragraph buildNote(String markerText, String content) {
      Chunk markerChunk = new Chunk(markerText, footnoteFont);
      markerChunk.setTextRise(4.0F);
      Chunk contentChunk = new Chunk(content, smallFont);
      Paragraph paragraph = new Paragraph();
      paragraph.setIndentationLeft(7.0F);
      paragraph.setFirstLineIndent(-7.F);
      paragraph.setLeading(12.0F, 0.0F);
      paragraph.add(markerChunk);
      paragraph.add(contentChunk);
      return paragraph;
   }
}

