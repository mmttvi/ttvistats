/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

/**
 * TtviStatsException
 */
public class TtviStatsException extends RuntimeException {

   public TtviStatsException(String message) {
      super(message);
   }

   public TtviStatsException(Throwable cause) {
      super(cause);
   }

}
