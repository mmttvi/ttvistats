/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.*;
import org.librepdf.openpdf.fonts.Liberation;

import java.io.IOException;

/**
 * PageWaterMark
 *
 */
public class PageWaterMark extends PdfPageEventHelper {

   private PdfGState gState;

   private final Font watermarkFont;
   private final String watermark;

   public PageWaterMark(String watermark) {
      try {
         this.watermark = watermark;
         watermarkFont = Liberation.SANS_BOLD.create(50);
      } catch (IOException e) {
         throw new TtviStatsException(e);
      }
   }

   @Override
   public void onOpenDocument(PdfWriter writer, Document document) {
      gState = new PdfGState();
      gState.setFillOpacity(0.2F);
      gState.setStrokeOpacity(0.2F);
   }

   @Override
   public void onEndPage(PdfWriter writer, Document document) {
      PdfContentByte directContent = writer.getDirectContent();
      directContent.saveState();
      directContent.setGState(gState);
      directContent.beginText();
      directContent.setFontAndSize(watermarkFont.getBaseFont(), 70F);
      directContent.showTextAligned(Element.ALIGN_CENTER, watermark, document.getPageSize().getWidth() / 2,
            document.getPageSize().getHeight() / 2, 45);
      directContent.endText();
      directContent.restoreState();
   }
}
