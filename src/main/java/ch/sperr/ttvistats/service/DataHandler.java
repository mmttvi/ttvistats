/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import ch.sperr.ttvistats.clickttclient.GroupPageParser;
import ch.sperr.ttvistats.clickttclient.GroupTableProxy;
import ch.sperr.ttvistats.clickttclient.LeaguePageParser;
import ch.sperr.ttvistats.data.TeamData;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Singleton;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.transaction.Transactional;
import javax.ws.rs.core.GenericType;
import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

@Singleton
public class DataHandler {
   @ConfigProperty(name = "ttvi.lowest.league", defaultValue = "4")
   int lowestLeague;

   public static final Type LIST_OF_TABLELINE_TYPE = new GenericType<List<TeamData>>() {}.getType();

   @RestClient
   GroupTableProxy groupTableProxy;

   @Transactional
   public String importData(String region, int year) {
      String resultPeriod = year + "/" + (year + 1);

      Map<String, Long> groups = LeaguePageParser.extractGroups(
            groupTableProxy.getLeagueHtml(region + " " + resultPeriod, "German"));

      long deleted = TeamData.deleteAll();
      Date fetchDate = new Date();

      TeamData.persist(
            groups.entrySet().stream()
                  .filter(e -> !e.getKey().endsWith("ES"))
                  .filter(e -> e.getKey().startsWith("He")
                        || e.getKey().startsWith("O40")
                        || e.getKey().startsWith("O50")
                        || e.getKey().startsWith("FSL")
                        || e.getKey().startsWith("Da"))
                  .flatMap(entry ->
                        GroupPageParser.extractTable(
                              groupTableProxy.getGroupHtml(region + " " + resultPeriod,
                                    entry.getValue(),
                                    "German"),
                              entry.getValue(),
                              entry.getKey(), lowestLeague).stream()
                  )
                  .map(TeamData::calculateRankingIndices)
                  .map(l -> {
                     l.fetchDate = fetchDate;
                     return l;
                  }));

      long inserted = TeamData.count();
      return "Imported data(deleted: " + deleted + " inserted: " + inserted + ")";
   }

   @Transactional(Transactional.TxType.SUPPORTS)
   public String getDataAsJson() {
      try (Jsonb jsonb = JsonbBuilder.create()) {
         List<TeamData> data = TeamData.findAll().stream().map(TeamData.class::cast).collect(Collectors.toList());
         return jsonb.toJson(data);
      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Transactional(Transactional.TxType.SUPPORTS)
   public List<TeamData> getDataAsList() {
      return TeamData.findAll().stream().map(TeamData.class::cast).collect(Collectors.toList());
   }

   @Transactional(Transactional.TxType.SUPPORTS)
   public boolean isEmpty() {
      return TeamData.count() == 0;
   }

   @Transactional(Transactional.TxType.SUPPORTS)
   public byte[] getDataAsZippedByteArray() {
      byte[] result;
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
         GZIPOutputStream gzipOutputStream = new GZIPOutputStream(bos);
         OutputStreamWriter osw = new OutputStreamWriter(gzipOutputStream, StandardCharsets.UTF_8);
         osw.write(getDataAsJson());
         osw.flush();
         gzipOutputStream.finish();
         result = bos.toByteArray();
      } catch (IOException ex) {
         throw new TtviStatsException(ex);
      }
      return result;
   }

   @Transactional
   public String loadDataFromGzippedInputStream(InputStream inputStream) {

      try (InputStreamReader inputStreamReader = new InputStreamReader(
            new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
           Jsonb jsonb = JsonbBuilder.create()) {

         List<TeamData> teamData = jsonb.fromJson(inputStreamReader, LIST_OF_TABLELINE_TYPE);
         long deleted = TeamData.deleteAll();

         teamData.stream().forEach(t -> TeamData.getEntityManager().merge(t));
         return "done " + teamData.size() + " lines imported (" + deleted + " deleted)";

      } catch (Exception e) {
         throw new TtviStatsException(e);
      }
   }

   @Transactional
   public void commitTeamData(TeamData entity) {
      TeamData.getEntityManager().merge(entity);
   }

   @Transactional
   public void commitTeamData(List<TeamData> entities) {
      entities.stream().forEach(this::commitTeamData);
   }

   @Transactional
   public void removeTeamData(TeamData teamData) {
      teamData.delete();
   }
}