/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.service;

import ch.sperr.ttvistats.data.TeamData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * TeamCountHelper
 */
public class TeamCountHelper {

   private final HashMap<String, Integer> teamCounts;
   private final int maximumTeamsPerGroup;
   private final List<Integer> maxGroups;


   public TeamCountHelper(List<TeamData> data, int maximumTeamsPerGroup) {

      this.maximumTeamsPerGroup = maximumTeamsPerGroup;

      teamCounts = new HashMap<>();
      data.stream()
            .map(this::buildKey)
            .forEach(s -> {
               Integer actualCount = teamCounts.getOrDefault(s, 0);
               teamCounts.put(s, ++actualCount);
            });
      int highestLeague = data.stream().map(l -> l.league).sorted(Comparator.reverseOrder()).findFirst().orElse(0);
      maxGroups = new ArrayList<>();
      maxGroups.add(4); // 4 NLC - Groups but this should be irrelevant here
      maxGroups.add(1);
      for (int i = 2; i <= highestLeague; i++) {
         maxGroups.add(maxGroups.get(maxGroups.size()-1) * 2);
      }
   }


   public boolean isTeamOfClubAllowedInLeague(String club, int league){
      return maximumTeamsPerGroup * maxGroups.get(league) > getTeamCount(club, league);
   }

   public int getTeamCount(String club, int league){
      return teamCounts.getOrDefault(buildKey(league, club), 0);
   }

   public void moveTeam(String club, int sourceLeague, int targetLeague){
      String sourceKey = buildKey(sourceLeague, club);
      String targetKey = buildKey(targetLeague,club);

      teamCounts.put(sourceKey, teamCounts.getOrDefault(sourceKey, 0) - 1);
      teamCounts.put(targetKey, teamCounts.getOrDefault(targetKey, 0) + 1);
   }

   public void addAdditionalTeamToLeague(String club, int league) {
      String key = buildKey(league, club);
      Integer actualCount = teamCounts.getOrDefault(key, 0);
      teamCounts.put(key, ++actualCount);
   }

   public Optional<String> getFirstCompromisingTeamInLeague(int league) {
      int maxTeamCountInLeague = maximumTeamsPerGroup * maxGroups.get(league);
      return teamCounts.entrySet().stream()
            .filter(e -> e.getKey().startsWith(league + "-"))
            .filter(e -> e.getValue().compareTo(maxTeamCountInLeague) > 0)
            .map(Map.Entry::getKey)
            .map(s -> s.replaceFirst("\\d+-", ""))
            .findFirst();
   }

   private String buildKey(TeamData l) {
      return buildKey(l.league, l.determineClub());
   }

   private String buildKey(int league, String club) {
      return league + "-" + club;
   }

   public int getLeagueTeamCount(int league) {
      String keyPrefix = league + "-";
      return teamCounts.entrySet().stream()
            .filter(e -> e.getKey().startsWith(keyPrefix))
            .collect(Collectors.summingInt(Map.Entry::getValue));
   }

   public Map<String, Integer> getState() {
      return new HashMap<>(teamCounts);
   }

   public void resetState(Map<String, Integer> state) {
      teamCounts.clear();
      teamCounts.putAll(state);
   }

}
