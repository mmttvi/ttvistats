/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.data.TeamData;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.ValueProvider;

import java.text.NumberFormat;
import java.util.List;

/**
 * TeamGrid
 */
@CssImport(value = "./styles/styles.css", themeFor = "vaadin-grid")
public class TeamGrid extends Grid<TeamData> {

   public TeamGrid(List<TeamData> teams) {
      setHeight("800px");
      setMaxHeight("800px");
      setSelectionMode(SelectionMode.SINGLE);
      setItems(teams);

      addColumn((ValueProvider<TeamData, String>) (TeamData t) -> t.series.getDescription())
            .setHeader("Serie").setKey("SERIES").setSortable(true).setWidth("6em").setFlexGrow(0).setResizable(true);
      addColumn((ValueProvider<TeamData, Integer>) (TeamData t) -> t.league)
            .setHeader("Liga").setKey("LEAGUE").setSortable(true).setWidth("6em").setFlexGrow(0).setResizable(true);
      addColumn((ValueProvider<TeamData, Integer>) TeamData::determineNextYearsLeague)
            .setHeader("Liga n.S.").setKey("LEAGUE_NEXT_YEAR").setSortable(true).setWidth("6em").setFlexGrow(0).setResizable(true);
      addColumn((ValueProvider<TeamData, Integer>) (TeamData t) -> t.league - t.determineNextYearsLeague())
            .setHeader("Auf/Ab").setKey("LEAGUE_DIFF").setSortable(true).setWidth("6em").setFlexGrow(0).setResizable(true);
      addColumn((ValueProvider<TeamData, String>) (TeamData t) -> t.groupName)
            .setHeader("Gruppe").setKey("GROUP").setSortable(true).setWidth("10em").setFlexGrow(0).setResizable(true);
      addColumn((ValueProvider<TeamData, String>) (TeamData t) -> t.teamName)
            .setHeader("Mannschaft").setKey("TEAM").setSortable(true).setResizable(true);

      addColumn((ValueProvider<TeamData, String>) TeamData::determinePromotionType)
            .setHeader("Aufstieg").setKey("PROM").setWidth("6em").setFlexGrow(0).setResizable(true);
      addColumn((ValueProvider<TeamData, String>) TeamData::determineRelegationType)
            .setHeader("Abstieg").setKey("REL").setWidth("6em").setFlexGrow(0).setResizable(true);

      addColumn((ValueProvider<TeamData, Integer>) (TeamData t) -> t.clickTtRank)
            .setHeader("Rang").setKey("RANK_IN_GROUP").setSortable(true).setResizable(true);
      addColumn((ValueProvider<TeamData, String>) TeamData::determineFormattedGroupRankingIndex)
            .setHeader("GRI").setKey("GRI").setComparator((TeamData t) -> t.groupRankingIndex).setResizable(true);
      NumberFormat numberFormat = NumberFormat.getNumberInstance();
      numberFormat.setMaximumFractionDigits(3);
      numberFormat.setMinimumFractionDigits(3);
      addColumn(new NumberRenderer<>((TeamData t) -> t.leagueRankingIndex, numberFormat))
            .setHeader("LRI").setKey("LRI").setTextAlign(ColumnTextAlign.END)
            .setComparator((TeamData t) -> t.leagueRankingIndex).setResizable(true);

      setMultiSort(true, MultiSortPriority.APPEND);
      setColumnReorderingAllowed(true);
      addThemeVariants(GridVariant.LUMO_ROW_STRIPES);

      getColumnByKey("PROM").setClassNameGenerator(buildClassNameGenerator());
      getColumnByKey("REL").setClassNameGenerator(buildClassNameGenerator());
      getColumnByKey("TEAM").setClassNameGenerator(buildClassNameGenerator());
   }
   private SerializableFunction<TeamData, String> buildClassNameGenerator() {
      return tableLine -> {
         String result = null;
         if (tableLine.isPromotedRegularly) {
            result = "regular-promotion";
         }
         if (tableLine.isPromotedAdditional) {
            result = "additional-promotion";
         }
         if (tableLine.isRelegatedRegularly) {
            result = "regular-relegation";
         }
         if (tableLine.isRelegatedAdditional) {
            result = "additional-relegation";
         }
         if (tableLine.isAllowedForNationalLeaguePromotion) {
            result = "allowed-for-nlc-promotion";
         }
         if (tableLine.isRetreatedByClub) {
            result = "retreated";
         }
         return result;
      };
   }



}
