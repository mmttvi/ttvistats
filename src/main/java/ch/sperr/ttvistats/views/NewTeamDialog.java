/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.data.Series;
import ch.sperr.ttvistats.data.TeamData;
import ch.sperr.ttvistats.service.DataHandler;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.quarkus.annotation.UIScoped;

import javax.inject.Inject;

/**
 * EditTeamDialog
 */
@UIScoped
public class NewTeamDialog extends Dialog {


   @Inject
   DataHandler dataHandler;

   private final TextField teamNameField;

   private transient Runnable doOnClose;

   public NewTeamDialog() {
      setHeaderTitle("New Relegated Team from NLC");

      addDialogCloseActionListener(event -> {});

      Button saveButton = new Button("Save", e -> {
         if (doOnClose != null) {
            doOnClose.run();
         }
         this.close();});
      Button cancelButton = new Button("Cancel", e -> this.close());

      VerticalLayout verticalLayout = new VerticalLayout();
      teamNameField = new TextField("TeamName", "Absteigendes Team");
      verticalLayout.add(teamNameField);
      add(verticalLayout);
      getFooter().add(saveButton, cancelButton);
   }

   public void addRelegatedTeam() {
      TeamData teamData = new TeamData();
      teamNameField.setValue("");
      doOnClose = () -> saveChangedData(teamData);
      open();
   }

   private void saveChangedData(TeamData teamData) {
      teamData.league = 1;
      teamData.groupId = determineGroupId();
      teamData.groupName = determineGroupName();
      teamData.teamName = teamNameField.getValue();
      teamData.clickTtRank = 0;
      teamData.isInPromotionalLeague = true;
      teamData.isPromotionProhibited = true;
      teamData.leagueRankingIndex = 1E50D;
      teamData.groupRankingIndex = Long.MAX_VALUE;
      teamData.series = Series.HERREN;
      teamData.isRelegatedFromNLC = true;
      dataHandler.commitTeamData(teamData);
   }

   private long determineGroupId() {
      return dataHandler.getDataAsList().stream()
            .filter(teamData -> Series.HERREN.equals(teamData.series))
            .filter(teamData -> teamData.league == 1)
            .findFirst()
            .orElseThrow()
            .groupId;
   }

   private String determineGroupName() {
      return dataHandler.getDataAsList().stream()
            .filter(teamData -> Series.HERREN.equals(teamData.series))
            .filter(teamData -> teamData.league == 1)
            .findFirst()
            .orElseThrow()
            .groupName;
   }

}
