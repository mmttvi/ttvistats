/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.data.TeamData;
import ch.sperr.ttvistats.service.DataHandler;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.quarkus.annotation.UIScoped;

import javax.inject.Inject;

/**
 * EditTeamDialog
 */
@UIScoped
public class EditTeamDialog extends Dialog {


   @Inject
   DataHandler dataHandler;

   private final Text teamName;
   private final Checkbox cbDeclinePromotion;
   private final Checkbox cbFacultativeRelegation;
   private final Checkbox cbRetreat;

   private final Checkbox cbAllowRelegationOmittance;
   private final Checkbox cbPromotedToNLC;
   private final Checkbox cbPromotedRegular;
   private final Checkbox cbPromotedAdditional;
   private final Checkbox cbRelegatedRegular;
   private final Checkbox cbRelegatedAdditional;


   private transient Runnable doOnClose;

   public EditTeamDialog () {
      setHeaderTitle("Edit Team Data");

      addDialogCloseActionListener(event -> {});

      Button saveButton = new Button("Save", e -> {
         if (doOnClose != null) {
            doOnClose.run();
         }
         this.close();});
      Button cancelButton = new Button("Cancel", e -> this.close());

      VerticalLayout verticalLayout = new VerticalLayout();
      teamName = new Text("unknown Team");
      HorizontalLayout row1 = new HorizontalLayout();
      cbDeclinePromotion = new Checkbox("Verz. auf Aufstieg", false);
      cbFacultativeRelegation = new Checkbox("Freiw. Abstieg", false);
      cbRetreat = new Checkbox("Rückzug", false);
      cbAllowRelegationOmittance = new Checkbox("Verzicht Abstieg", false);
      row1.add(cbDeclinePromotion, cbFacultativeRelegation, cbRetreat, cbAllowRelegationOmittance);

      HorizontalLayout row2 = new HorizontalLayout();
      cbPromotedToNLC = new Checkbox("Aufstieg in NLC", false);
      cbPromotedRegular = new Checkbox("Aufstieg (regulär)", false);
      cbPromotedAdditional = new Checkbox("Aufstieg (zusätzlich)", false);
      row2.add(cbPromotedToNLC, cbPromotedRegular, cbPromotedAdditional);

      HorizontalLayout row3 = new HorizontalLayout();
      cbRelegatedRegular = new Checkbox("Abstieg (regulär)", false);
      cbRelegatedAdditional = new Checkbox("Abstieg (zusätzlich)", false);
      row3.add(cbRelegatedRegular, cbRelegatedAdditional);


      verticalLayout.add(teamName, row1, row2, row3);
      add(verticalLayout);
      getFooter().add(saveButton, cancelButton);
   }

   public void editTeam(TeamData teamData) {
      if (teamData != null) {
         teamName.setText(teamData.teamName);
         cbDeclinePromotion.setValue(teamData.isPromotionDeclinedByClub);
         cbFacultativeRelegation.setValue(teamData.isRelegatedByClub);
         cbRetreat.setValue(teamData.isRetreatedByClub);
         cbAllowRelegationOmittance.setValue(teamData.isOmittanceOfRelegationAllowedByClub);
         cbPromotedToNLC.setValue(teamData.isPromotedToNLC);
         cbPromotedRegular.setValue(teamData.isPromotedRegularly);
         cbPromotedAdditional.setValue(teamData.isPromotedAdditional);
         cbRelegatedRegular.setValue(teamData.isRelegatedRegularly);
         cbRelegatedAdditional.setValue(teamData.isRelegatedAdditional);
         doOnClose = () -> saveChangedData(teamData);
      }
      open();
   }

   private void saveChangedData(TeamData teamData) {
      teamData.isPromotionDeclinedByClub = cbDeclinePromotion.getValue();
      teamData.isRelegatedByClub = cbFacultativeRelegation.getValue();
      teamData.isRetreatedByClub = cbRetreat.getValue();
      teamData.isOmittanceOfRelegationAllowedByClub = cbAllowRelegationOmittance.getValue();
      teamData.isPromotedToNLC = cbPromotedToNLC.getValue();
      teamData.isPromotedRegularly = cbPromotedRegular.getValue();
      teamData.isPromotedAdditional = cbPromotedAdditional.getValue();
      teamData.isRelegatedRegularly = cbRelegatedRegular.getValue();
      teamData.isRelegatedAdditional = cbRelegatedAdditional.getValue();
      dataHandler.commitTeamData(teamData);
   }

}
