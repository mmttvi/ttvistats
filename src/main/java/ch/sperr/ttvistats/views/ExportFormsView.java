/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.service.FormsGenerator;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.quarkus.annotation.UIScoped;
import org.vaadin.olli.FileDownloadWrapper;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ExportFormsView
 *
 */
@UIScoped
public class ExportFormsView extends VerticalLayout {

   @Inject
   FormsGenerator formsGenerator;

   private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
   @PostConstruct
   void init(){

      VerticalLayout layout = new VerticalLayout();

      Button firstFormsButton = new Button("Generate first set of forms");
      String proposedFileName = "ttvi-forms-1-" + dateFormat.format(new Date()) + ".pdf";
      StreamResource downloadStream = new StreamResource(proposedFileName, () -> formsGenerator.generateFirstFormSet());
      FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(downloadStream);
      buttonWrapper.wrapComponent(firstFormsButton);
      layout.add(buttonWrapper);

      Button firstFormsAsZipButton = new Button("Generate first set of forms as Zip");
      String proposedZipFileName = "ttvi-forms-1-" + dateFormat.format(new Date()) + ".zip";
      StreamResource downloadZipStream = new StreamResource(proposedZipFileName, () -> formsGenerator.generateFirstFormSetAsZip());
      FileDownloadWrapper buttonWrapperZip = new FileDownloadWrapper(downloadZipStream);
      buttonWrapperZip.wrapComponent(firstFormsAsZipButton);
      layout.add(buttonWrapperZip);


      Button secondFormsButton = new Button("Generate second set of forms");
      String proposedFileName2 = "ttvi-forms-2-" + dateFormat.format(new Date()) + ".pdf";
      StreamResource downloadStream2 = new StreamResource(proposedFileName2, () -> formsGenerator.generateSecondFormSet());
      FileDownloadWrapper buttonWrapper2 = new FileDownloadWrapper(downloadStream2);
      buttonWrapper2.wrapComponent(secondFormsButton);
      layout.add(buttonWrapper2);

      Button secondFormsAsZipButton = new Button("Generate second set of forms as Zip");
      String proposedZipFileName2 = "ttvi-forms-2-" + dateFormat.format(new Date()) + ".zip";
      StreamResource downloadZipStream2 = new StreamResource(proposedZipFileName2, () -> formsGenerator.generateSecondFormSetAsZip());
      FileDownloadWrapper buttonWrapperZip2 = new FileDownloadWrapper(downloadZipStream2);
      buttonWrapperZip2.wrapComponent(secondFormsAsZipButton);
      layout.add(buttonWrapperZip2);

      add(layout);
   }
}
