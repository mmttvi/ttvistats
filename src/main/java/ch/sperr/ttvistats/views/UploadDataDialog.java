/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.service.DataHandler;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;

import java.io.InputStream;

/**
 * UploadDataDialog
 */
public class UploadDataDialog extends Dialog {


   private final transient DataHandler dataHandler;

   private final MultiFileMemoryBuffer receiver;

   public UploadDataDialog(DataHandler dataHandler) {
      this.dataHandler = dataHandler;
      receiver = new MultiFileMemoryBuffer();

      Upload dataUpload = new Upload(receiver);
      dataUpload.setMaxFiles(1);

      setHeaderTitle("Daten hochladen");

      addDialogCloseActionListener(event -> {});

      dataUpload.addSucceededListener( event -> {
         String fileName = event.getFileName();
         InputStream inputStream = receiver.getInputStream(fileName);
         String resultString = this.dataHandler.loadDataFromGzippedInputStream(inputStream);
         Notification notification = new Notification(resultString, 10000);
         notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
         notification.open();
      });


      Button closeButton = new Button("Schliessen", e -> this.close());

      VerticalLayout verticalLayout = new VerticalLayout();
      verticalLayout.add(dataUpload, closeButton);
      this.add(verticalLayout);
   }
}
