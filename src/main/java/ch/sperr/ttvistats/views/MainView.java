/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.service.DataHandler;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


/**
 * MainView
 */
@Route("")
public class MainView extends AppLayout {

   public static final String PROMOTIONS_RELEGATIONS_TAB = "Promotions/Relegations";
   public static final String FORMS_TAB = "FormsTab";
   public static final String DATA_TAB = "DataTab";
   private final Tabs tabs;

   @Inject
   DataView dataView;
   @Inject
   PromotionRelegationView promotionRelegationView;
   @Inject
   ExportFormsView exportFormsView;
   @Inject
   DataHandler dataHandler;

   public MainView() {
      DrawerToggle drawerToggle = new DrawerToggle();

      H1 title = new H1("TTVI Saisonabschluss Helfer");
      title.getStyle()
            .set("font-size", "var(--lumo-font-size-1)")
            .set("margin", "0");

      tabs = getTabs();

      addToDrawer(tabs);
      addToNavbar(drawerToggle, title);
   }

   @PostConstruct
   public void setInitialContent() {
      if (dataHandler.isEmpty()) {
         tabs.setSelectedIndex(0);
         setContent(dataView);
      } else {
         tabs.setSelectedIndex(1);
         setContent(promotionRelegationView);
      }
   }


   private Tabs getTabs() {

      Tabs newTabs = new Tabs();
      newTabs.setOrientation(Tabs.Orientation.VERTICAL);
      Tab firstTab = new Tab();
      firstTab.setLabel("Daten Im-/Export");
      firstTab.setId(DATA_TAB);
      newTabs.add(firstTab);


      Tab promotionRelegationTab = new Tab("Auf-/Abstiege");
      promotionRelegationTab.setId(PROMOTIONS_RELEGATIONS_TAB);
      newTabs.add(promotionRelegationTab);

      Tab generateFormsTab = new Tab("Formulare");
      generateFormsTab.setId(FORMS_TAB);
      newTabs.add(generateFormsTab);


      newTabs.addSelectedChangeListener(event -> {
         Tab selectedTab = event.getSelectedTab();
         String id = selectedTab.getId().orElse("");

         switch (id) {
            case DATA_TAB:
               setContent(dataView);
               break;
            case PROMOTIONS_RELEGATIONS_TAB:
               setContent(promotionRelegationView);
               break;
            case FORMS_TAB:
               setContent(exportFormsView);
               break;
            default: setContent(new Label("unknown tab"));
         }
      });
      return newTabs;
   }
}