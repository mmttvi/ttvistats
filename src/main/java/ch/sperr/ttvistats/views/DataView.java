/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.service.DataHandler;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.quarkus.annotation.UIScoped;
import org.vaadin.olli.FileDownloadWrapper;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * ImportDataView
 */
@UIScoped
public class DataView extends VerticalLayout {

   private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
   private final FetchDataForm fetchDataForm = new FetchDataForm();
   private final TextArea rawDataArea;

   @Inject
   DataHandler dataHandler;

   public DataView() {
      rawDataArea = new TextArea("Rohdaten");
      rawDataArea.setWidthFull();
   }

   @PostConstruct
   public void init() {
      Details details1 = new Details();

      VerticalLayout fetchDataPanel = buildFetchDataPanel();
      details1.setSummaryText("Daten von click-tt holen");
      details1.addContent(fetchDataPanel);
      details1.setOpened(true);

      Button downLoadButton = new Button("Daten speichern");
      String proposedFileName = "ttvi-data-" + dateFormat.format(new Date()) + ".gz";
      StreamResource downloadStream = new StreamResource(proposedFileName, () -> new ByteArrayInputStream(dataHandler.getDataAsZippedByteArray()));
      FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(downloadStream);
      buttonWrapper.wrapComponent(downLoadButton);

      Button uploadDataButton = new Button("Daten hochladen");
      uploadDataButton.addClickListener(event -> {
         UploadDataDialog uploadDataDialog = new UploadDataDialog(dataHandler);
         uploadDataDialog.open();
      });

      Button showDataButton = new Button("Rohdaten anzeigen");
      showDataButton.addClickListener(event -> rawDataArea.setValue(dataHandler.getDataAsJson()));
      VerticalLayout saveAndRestorePanel = new VerticalLayout();
      saveAndRestorePanel.add(buttonWrapper, uploadDataButton);

      Details details2 = new Details();
      details2.setSummaryText("Daten speichern und laden");
      details2.setContent(saveAndRestorePanel);
      details2.setOpened(true);

      VerticalLayout rawDataPanel = new VerticalLayout();
      Details details3 = new Details();
      details3.setSummaryText("Rohdaten zeigen");
      rawDataPanel.add(showDataButton, rawDataArea);
      details3.setContent(rawDataPanel);
      details3.setWidthFull();

      addClassName("centered-content");
      add(details1, details2, details3);

   }



   private VerticalLayout buildFetchDataPanel() {
      Button button = new Button("Von click-tt holen",
            e -> { String result = dataHandler.importData(fetchDataForm.getAssociation(),
                  fetchDataForm.getYear() % 100);
               Notification notification = new Notification(result, 10000);
               notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
               notification.open();
      });
      button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
      button.addClickShortcut(Key.ENTER);

      VerticalLayout fetchDataPanel = new VerticalLayout();
      fetchDataPanel.add(fetchDataForm, button);
      return fetchDataPanel;
   }
}
