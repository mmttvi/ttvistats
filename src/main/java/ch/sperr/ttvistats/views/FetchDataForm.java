/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.select.Select;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

/**
 * FetchDataForm: form to fetch data from click-tt
 */
public class FetchDataForm extends FormLayout{

   private final Select<Integer> yearSelect;
   private final Select<String> associationSelect;

   public FetchDataForm() {

      this.setResponsiveSteps(
            new FormLayout.ResponsiveStep("0", 1),
            new FormLayout.ResponsiveStep("320px", 2),
            new FormLayout.ResponsiveStep("500px", 3)
      );

      int currentYear = LocalDate.now().get(ChronoField.YEAR);
      yearSelect = new Select<>();
      yearSelect.setLabel("Saison (Startjahr)");
      yearSelect.setItems(currentYear-2, currentYear-1, currentYear);
      yearSelect.setValue(currentYear-1);

      this.add(yearSelect);

      associationSelect = new Select<>();
      associationSelect.setLabel("Verband");
      associationSelect.setItems("TTVI");
      associationSelect.setValue("TTVI");

      this.add(associationSelect);
   }

   public int getYear() {
      return yearSelect.getValue();
   }

   public String getAssociation() {
      return associationSelect.getValue();
   }
}
