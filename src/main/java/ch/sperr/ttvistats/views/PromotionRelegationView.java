/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.views;

import ch.sperr.ttvistats.data.Series;
import ch.sperr.ttvistats.data.TeamData;
import ch.sperr.ttvistats.service.DataHandler;
import ch.sperr.ttvistats.service.PromotionRelegationService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.quarkus.annotation.UIScoped;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * PromotionRelegationView
 */
@UIScoped
public class PromotionRelegationView extends VerticalLayout {

   @Inject
   DataHandler dataHandler;

   @Inject
   EditTeamDialog editTeamDialog;

   @Inject
   NewTeamDialog newRelegatedNlcTeamDialog;

   @Inject
   PromotionRelegationService promotionRelegationService;

   private TeamGrid grid;

   @PostConstruct
   public void init() {

      editTeamDialog.addOpenedChangeListener(event -> {
         if (!event.isOpened()) {
            checkAndCommitAllChanges();
            //promotionRelegationService.calculateStandardPromotionAndRelegation();
            //refreshGrid();
         }
      });

      newRelegatedNlcTeamDialog.addOpenedChangeListener(event -> {
         if (!event.isOpened()) {
            promotionRelegationService.calculateStandardPromotionAndRelegation();
            refreshGrid();
         }
      });

      Button calculatePromotionsAndRelegationsButton = new Button("Auf-/Abstiege berechnen",
            event -> {
               promotionRelegationService.calculateStandardPromotionAndRelegation();
               refreshGrid();
            });

      Button showDataButton = new Button("Daten anzeigen",
            event -> refreshGrid());

      Button editCurrentRowButton = new Button("Aktuellen Datensatz editieren",
            event -> editCurrentTeam());

      Button addRelegatedNlcTeamButton = new Button("Neuer NLC Absteiger",
            event -> addRelegatedNLCTeam());

      Button removeRelegatedNlcTeamButton = new Button("NLC Absteiger entfernen",
            event -> removeRelegatedNLCTeam());

      Button commitChangesButton = new Button("Rückzüge prüfen",
            event -> checkAndCommitAllChanges());

      grid = new TeamGrid(Collections.emptyList());
      grid.addItemDoubleClickListener(event -> editTeam(event.getItem()));
      HorizontalLayout firstButtonLine = new HorizontalLayout();
      firstButtonLine.add(showDataButton, calculatePromotionsAndRelegationsButton, editCurrentRowButton);
      HorizontalLayout secondButtonLine = new HorizontalLayout();
      secondButtonLine.add(addRelegatedNlcTeamButton,
            removeRelegatedNlcTeamButton, commitChangesButton);
      add(firstButtonLine, secondButtonLine, grid);

      if (!dataHandler.isEmpty()) {
         refreshGrid();
      }

      addAttachListener(event -> {
         if (!dataHandler.isEmpty()) {
            refreshGrid();
         }
      });

   }

   private void refreshGrid() {
      List<TeamData> data = getSortedData();
      grid.setItems(data);
   }

   private void removeRelegatedNLCTeam() {
      TeamData teamData = grid.getSelectedItems().stream().findFirst().orElse(null);
      if (teamData != null && teamData.isRelegatedFromNLC) {
         dataHandler.removeTeamData(teamData);
      }
      promotionRelegationService.calculateStandardPromotionAndRelegation();
      refreshGrid();
   }

   private void addRelegatedNLCTeam() {
      newRelegatedNlcTeamDialog.addRelegatedTeam();
   }

   public void checkAndCommitAllChanges() {
      List<TeamData> allTeams = grid.getGenericDataView().getItems().collect(Collectors.toList());
      checkRetreats(allTeams);
      dataHandler.commitTeamData(allTeams);
      promotionRelegationService.calculateStandardPromotionAndRelegation();
      refreshGrid();
   }

   private static void checkRetreats(List<TeamData> allTeams) {
      HashMap<String, Integer> retreatedLeagueMap = new LinkedHashMap<>();

      allTeams.stream()
            .sorted(TeamData.getStandardComparator())
            .filter(team -> team.isRetreatedByClub)
            .map(team -> new ImmutablePair<>(team.determineClub(), team.league))
            .forEach(pair -> {
               Integer currentLeague = retreatedLeagueMap.get(pair.left);
               Integer newLeague = currentLeague == null || currentLeague > pair.getRight()
                     ? pair.getRight()
                     : currentLeague;
               retreatedLeagueMap.put(pair.getLeft(), newLeague);
            });

      List<TeamData> autoRetreatedTeams = new ArrayList<>();

      allTeams.stream()
            .filter(teamData -> retreatedLeagueMap.containsKey(teamData.determineClub()))
            .filter(teamData -> retreatedLeagueMap.get(teamData.determineClub()) < teamData.league)
            .filter(teamData -> !teamData.isRetreatedByClub)
            .forEach(teamData -> {
               autoRetreatedTeams.add(teamData);
               teamData.isRetreatedByClub = true;
            });

      if (!autoRetreatedTeams.isEmpty()) {
         String autoRetreatedTeamNames = autoRetreatedTeams.stream()
               .map(team -> team.teamName)
               .collect(Collectors.joining(",\n"));

         Dialog warnDialog = new Dialog();
         warnDialog.setHeaderTitle("WARNING");
         TextArea warning = new TextArea();
         warning.setValue("Folgende Mannschaften werden automatisch zurückgezogen!\n"
         + autoRetreatedTeamNames + "\n");
         warnDialog.add(warning);
         Button closebutton = new Button("Close", e -> warnDialog.close());
         warnDialog.getFooter().add(closebutton);
         warnDialog.open();
      }
   }

   public void editCurrentTeam() {
      TeamData teamData = grid.getSelectedItems().stream().findFirst().orElse(null);
      editTeamDialog.editTeam(teamData);
   }

   public void editTeam(TeamData item) {
      editTeamDialog.editTeam(item);
   }


   private List<TeamData> getSortedData() {
      List<TeamData> data = dataHandler.getDataAsList();
      data.sort((a, b) -> {
         int result = Series.getSortComparator().compare(a.series, b.series);
         if (result == 0) {
            result = Integer.compare(a.league, b.league);
         }
         if (result == 0) {
            result = a.groupName.compareTo(b.groupName);
         }
         if (result == 0) {
            result = Long.compare(b.groupRankingIndex, a.groupRankingIndex);
         }
         return result;
      });
      return data;
   }
}
