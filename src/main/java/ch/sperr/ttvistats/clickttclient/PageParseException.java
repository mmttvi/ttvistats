/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.clickttclient;

/**
 * PageParseException
 *
 */
public class PageParseException extends RuntimeException {

   public PageParseException(String message) {
      super(message);
   }
}
