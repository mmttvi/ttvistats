/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.clickttclient;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * GroupTableProxy
 */

@RegisterRestClient(baseUri = "https://click-tt.ch/cgi-bin/WebObjects")
@Path("/nuLigaTTCH.woa/wa/")
public interface GroupTableProxy {

   @GET
   @Path("groupPage")
   @Produces(MediaType.TEXT_HTML)
   String getGroupHtml(
         @QueryParam("championship") String championship,
         @QueryParam("group") long group,
         @QueryParam("preferredLanguage") String language
   );


   @GET
   @Path("leaguePage")
   @Produces(MediaType.TEXT_HTML)
   String getLeagueHtml(
         @QueryParam("championship") String championship,
         @QueryParam("preferredLanguage") String language
   );
}
