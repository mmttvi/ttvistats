/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.clickttclient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * leagueParser
 */
public class LeaguePageParser {

   private static final Pattern groupPattern = Pattern.compile(".*group=(\\d+).*");

   private LeaguePageParser() {
      // do not instantiate
   }

   public static Map<String, Long> extractGroups(String leaguePageHtml) {

      Map<String, Long> result = new HashMap<>();
      Document document = Jsoup.parse(leaguePageHtml);

      Elements links = document.select("a[href]");

      for (Element link: links) {
         String href = link.attr("href");
         String text = link.text().trim();
         Matcher matcher = groupPattern.matcher(href);
         if (matcher.matches()) {
            Long group = Long.parseLong(matcher.group(1));
            result.put(text, group);
         }
      }
      return result;
   }

}
