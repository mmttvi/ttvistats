/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.clickttclient;

import ch.sperr.ttvistats.data.Series;
import ch.sperr.ttvistats.data.TeamData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * GroupPageParser
 */


public class GroupPageParser {
   private static final Pattern leaguePattern = Pattern.compile("(\\d+)\\.\\s*Liga");

   private GroupPageParser() {
      //inhibit instantiation
   }

   public static List<TeamData> extractTable(String leaguePageHtml, long groupId, String groupName, int lowestMensLeague) {

      Document document = Jsoup.parse(leaguePageHtml);
      List<TeamData> result = new ArrayList<>();

      Element table = document.select("h2:contains(Tabelle) + table").first();
      assert table != null;
      Elements tableRows = table.select("tr");

      for (Element row : tableRows) {

         if (row.select("th").isEmpty()) {
            TeamData line = new TeamData();
            line.groupId = groupId;
            line.groupName = groupName;
            line.series = extractSeries(groupName);
            line.league = extractLeague(groupName);

            if (line.series.equals(Series.HERREN)) {
               line.isInPromotionalLeague = true;
               line.isInRegisterLeague = line.league == lowestMensLeague;
            } else {
               line.isInPromotionalLeague = false;
               line.isInRegisterLeague = true;
            }

            Elements fields = row.select("td");

            Elements images = fields.get(0).select("img");
            if (!images.isEmpty()) {
               String altText = images.get(0).attr("alt");
               if (altText.equalsIgnoreCase("Aufsteiger")) {
                  line.clickTtPromotionState = TeamData.PromotionState.PROMOTION;
               } else if (altText.equalsIgnoreCase("Absteiger")) {
                  line.clickTtPromotionState = TeamData.PromotionState.RELEGATION;
               }
            }


            line.clickTtRank = Integer.parseInt(fields.get(1).text());

            Element teamField = fields.get(2);
            line.teamName = teamField.select("a").get(0).text();

            if (fields.size() > 5) {

               try {
                  line.nbrOfMatches = Integer.parseInt(fields.get(3).text());
                  line.nbrOfMatchesWon = Integer.parseInt(fields.get(4).text());
                  line.nbrOfMatchesTied = Integer.parseInt(fields.get(5).text());
                  line.nbrOfMatchesLost = Integer.parseInt(fields.get(6).text());
               } catch (NumberFormatException ex) {
                  line.nbrOfMatches = 0;
                  line.nbrOfMatchesWon = 0;
                  line.nbrOfMatchesTied = 0;
                  line.nbrOfMatchesLost = 0;
               }


               Element pointsDetailSpan = fields.get(7).select("span").get(0);

               String gameQuotientString = pointsDetailSpan.text();
               Quotient gameQuotient = new Quotient(gameQuotientString);
               line.nbrOfGamesWon = gameQuotient.getNumerator();
               line.nbrOfGamesLost = gameQuotient.getDenominator();
               String title = pointsDetailSpan.attr("title");
               String[] quotients = title.split("/");
               Quotient setQuotient = new Quotient(quotients[0]);
               Quotient ballQuotient = new Quotient(quotients[1]);
               line.nbrOfSetsWon = setQuotient.getNumerator();
               line.nbrOfSetsLost = setQuotient.getDenominator();
               line.nbrOfBallsWon = ballQuotient.getNumerator();
               line.nbrOfBallsLost = ballQuotient.getDenominator();

               Quotient pointQuotient = new Quotient(fields.get(9).text());
               line.nbrOfPointsWon = pointQuotient.getNumerator();
               line.nbrOfPointsLost = pointQuotient.getDenominator();
            }
            result.add(line);
         }
      }
      return result;
   }

   private static Series extractSeries(String groupName) {
      Series result = Series.UNKNOWN;
      if (groupName.startsWith(Series.HERREN.getPrefix())) {
         result = Series.HERREN;
      } else if (groupName.startsWith(Series.FSL.getPrefix())) {
         result = Series.FSL;
      } else if (groupName.startsWith(Series.O40.getPrefix())) {
         result = Series.O40;
      } else if (groupName.startsWith(Series.O50.getPrefix())) {
         result = Series.O50;
      } else if (groupName.startsWith(Series.DAMEN.getPrefix())) {
         result = Series.DAMEN;
      }
      return result;
   }

   private static int extractLeague(String groupName) {
      Matcher leagueMatcher = leaguePattern.matcher(groupName);
      if (leagueMatcher.find()) {
         return Integer.parseInt(leagueMatcher.group(1));
      }
      return 1;
   }

   private static class Quotient {
      private static final Pattern quotientPattern = Pattern.compile("(?:.*\\D)?(\\d+):(\\d+).*");

      private final int numerator;
      private final int denominator;

      public Quotient(String quotient) {
         Matcher matcher = quotientPattern.matcher(quotient);
         if (matcher.matches()) {
            numerator = Integer.parseInt(matcher.group(1));
            denominator = Integer.parseInt(matcher.group(2));
         } else {
            throw new PageParseException("could not parse quotient[" + quotient + "]");
         }
      }

      public int getNumerator() {
         return numerator;
      }

      public int getDenominator() {
         return denominator;
      }
   }


}
