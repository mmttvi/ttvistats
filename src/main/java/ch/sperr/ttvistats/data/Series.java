/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.data;

import java.util.Comparator;

/**
 * Series
 *
 */
public enum Series {
   HERREN("He", "Herren", 1),
   DAMEN("Da", "Damen", 3),
   FSL("FSL", "Freundschaftsliga", 2),
   O40("O40", "Senioren O40", 4),
   O50("O50", "Veteranen O50", 5),
   UNKNOWN("", "unbekannt", Integer.MAX_VALUE);


   private final String prefix;
   private final String description;

   private final int sortOrder;

   Series(String prefix, String description, int sortOrder) {
      this.prefix = prefix;
      this.description = description;
      this.sortOrder = sortOrder;
   }

   public String getPrefix() {
      return prefix;
   }

   public String getDescription() {
      return description;
   }

   public int getSortOrder() {
      return sortOrder;
   }

   public static Comparator<Series> getSortComparator() {
      return Comparator.comparingInt(Series::getSortOrder);
   }

}
