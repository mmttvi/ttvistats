/*
 * This file is licenced under the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
 *
 * Copyright 2023 by Christian Sperr
 *
 */

package ch.sperr.ttvistats.data;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TableLine; represents a line/team in a championship table
 */

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "Teams")
public class TeamData extends PanacheEntity {

   private static Map<String, Integer>  romanMap;

   static {
      romanMap = new HashMap<>();
      romanMap.put("I", 1);
      romanMap.put("II", 2);
      romanMap.put("III", 3);
      romanMap.put("IV", 4);
      romanMap.put("V", 5);
      romanMap.put("VI", 6);
      romanMap.put("VII", 7);
      romanMap.put("VIII", 8);
      romanMap.put("IX", 9);
      romanMap.put("X", 10);
      romanMap.put("XI", 11);
      romanMap.put("XII", 12);
      romanMap.put("XIII", 13);
      romanMap.put("XIV", 14);
      romanMap.put("XV", 15);
      romanMap.put("XVI", 16);
      romanMap.put("XVII", 17);
      romanMap.put("XVIII", 18);
      romanMap.put("XIX", 19);
      romanMap.put("XX", 20);
   }

   private static final Pattern GET_CLUB_PATTERN = Pattern.compile("^(.+?)( ([IVX]+))?$");

   public static final long SET_FACTOR = 10000;
   public static final long GAME_FACTOR = 1000;
   public static final long POINT_FACTOR = 1000;

   public enum PromotionState {
      PROMOTION, RELEGATION
   }

   public long groupId;
   public String groupName;
   public PromotionState clickTtPromotionState;
   public int clickTtRank;
   public String teamName;
   public int nbrOfMatches;
   public int nbrOfMatchesWon;
   public int nbrOfMatchesTied;
   public int nbrOfMatchesLost;
   public int nbrOfGamesWon;
   public int nbrOfGamesLost;
   public int nbrOfSetsWon;
   public int nbrOfSetsLost;
   public int nbrOfBallsWon;
   public int nbrOfBallsLost;
   public int nbrOfPointsWon;
   public int nbrOfPointsLost;


   @Enumerated(EnumType.STRING)
   public Series series;
   public int league;

   @JsonbNumberFormat("#0.000")
   public double leagueRankingIndex;
   public long groupRankingIndex;

   public boolean isPromotedRegularly;

   public boolean isPromotionProhibited;

   public boolean isPromotionDeclinedByClub;

   public boolean isRelegatedByClub;
   public boolean isRetreatedByClub;

   public boolean isPromotedAdditional;

   public boolean isRelegatedRegularly;

   public boolean isRelegationOmittable;

   public boolean isOmittanceOfRelegationAllowedByClub;

   public boolean isRelegatedAdditional;

   public boolean isAllowedForNationalLeaguePromotion;

   public boolean isParticipationInNLPromotionAcceptedByClub;

   public boolean isPromotedToNLC;

   public boolean isRelegatedFromNLC;

   public boolean isInPromotionalLeague;

   public boolean isInRegisterLeague;

   public Date fetchDate;


   public String determineClub() {
      if (teamName == null) {
         return "unknown club";
      }
      Matcher matcher = GET_CLUB_PATTERN.matcher(teamName);
      if (matcher.matches()) {
         return matcher.group(1);
      } else {
         return "unknown club";
      }
   }

   public int determineTeamNumber() {
      if (teamName == null) {
         return -1;
      }
      Matcher matcher = GET_CLUB_PATTERN.matcher(teamName);
      if (matcher.matches()) {
          String roman = matcher.group(3);
          if (roman == null || "".equals(roman)) {
             return 1;
          } else {
             return romanMap.get(roman);
          }
      } else {
         return -1;
      }
   }

   public TeamData calculateRankingIndices() {
      groupRankingIndex = ((nbrOfPointsWon * POINT_FACTOR
            + (nbrOfGamesWon - nbrOfGamesLost)) * GAME_FACTOR
            + (nbrOfSetsWon - nbrOfSetsLost)) * SET_FACTOR
            + (nbrOfBallsWon - nbrOfBallsLost);

      if (nbrOfMatches > 0) {
         leagueRankingIndex = (1.0 * groupRankingIndex) / nbrOfMatches;
      } else {
         leagueRankingIndex = -1000.0;
      }
      return this;
   }

   public String determineFormattedGroupRankingIndex() {
      return clickTtRank + ":"
            + nbrOfPointsWon
            + sign(nbrOfGamesWon - nbrOfGamesLost)
            + (nbrOfGamesWon - nbrOfGamesLost)
            + sign(nbrOfSetsWon - nbrOfSetsLost)
            + (nbrOfSetsWon - nbrOfSetsLost)
            + sign(nbrOfBallsWon - nbrOfBallsLost)
            + (nbrOfBallsWon - nbrOfBallsLost);
   }

   private String sign(int number) {
      return number >= 0 ? "+" : "";
   }

   @Override
   public String toString() {
      return "TableLine{" +
            "GroupId=" + groupId +
            ", Series=" + series +
            ", League=" + league +
            ", GroupName='" + groupName +
            ", TeamName='" + teamName +
            ", PromotionState(click-tt)=" + clickTtPromotionState +
            ", Rank(click-tt)=" + clickTtRank +
            ", PointsWon=" + nbrOfPointsWon +
            ", PointsLost=" + nbrOfPointsLost +
            ", Matches=" + nbrOfMatches +
            ", MatchesWon=" + nbrOfMatchesWon +
            ", MatchesTied=" + nbrOfMatchesTied +
            ", MatchesLost=" + nbrOfMatchesLost +
            ", GamesWon=" + nbrOfGamesWon +
            ", GamesLost=" + nbrOfGamesLost +
            ", SetsWon=" + nbrOfSetsWon +
            ", SetsLost=" + nbrOfSetsLost +
            ", BallsWon=" + nbrOfBallsWon +
            ", BallsLost=" + nbrOfBallsLost +
            ", GroupRankingIndex=" + groupRankingIndex +
            ", LeagueRankingIndex=" + leagueRankingIndex +
            ", isPromotedRegularly=" + isPromotedRegularly +
            ", isProhibitedForPromotion=" + isPromotionProhibited +
            ", isPromotionDeclined(Club)=" + isPromotionDeclinedByClub +
            ", isRetreated(Club)=" + isRetreatedByClub +
            ", isPromotedAdditional=" + isPromotedAdditional +
            ", isRelegatedRegularly=" + isRelegatedRegularly +
            ", isAllowedForNonRelegation=" + isRelegationOmittable +
            ", isOmittanceOfRelegationAllowed(Club)=" + isOmittanceOfRelegationAllowedByClub +
            ", isAllowedForNationalLeaguePromotion=" + isAllowedForNationalLeaguePromotion +
            ", isParticipationInNLPromotionAccepted(Club)=" + isParticipationInNLPromotionAcceptedByClub +
            ", isPromotedToNLC=" + isPromotedToNLC +
            ", isRelegatedFromNLC=" + isRelegatedFromNLC +
            ", isRelegatedByClub=" + isRelegatedByClub +
            ", isRelegationAdditional=" + isRelegatedAdditional +
            ", isInPromotionalLeague=" + isInPromotionalLeague +
            ", isInRegistrationLeague=" + isInRegisterLeague +
            ", fetchDate=" + fetchDate +
            '}';
   }

   public int determineNextYearsLeague() {
      if (isPromotedToNLC) {
         return 0;
      } else {
         return league
               + ((isPromotedRegularly || isPromotedAdditional) ? -1 : 0)
               + ((isRelegatedRegularly || isRelegatedAdditional) ? 1 : 0);
      }
   }

   public String determinePromotionType() {
      if (isPromotedRegularly) {
         return "R";
      } else if (isPromotedAdditional) {
         return "Z";
      } else if (isPromotedToNLC) {
         return "NLC";
      } else if (isPromotionDeclinedByClub) {
         return "V";
      } else {
         return "-";
      }
   }

   public String determineRelegationType() {
      if (isRelegatedRegularly) {
         return "R";
      } else if (isRelegatedAdditional) {
         if (isRelegatedByClub) {
            return "F";
         }
         return "Z";
      } else if (isRelegatedFromNLC) {
         return "NLC";
      } else if (isRelegationOmittable) {
         return "V";
      } else {
         return "-";
      }
   }

   public static Comparator<TeamData> getStandardComparator() {
      return (a, b) -> {
         int result = Series.getSortComparator().compare(a.series, b.series);
         if (result == 0) {
            result = a.determineClub().compareTo(b.determineClub());
         }
         if (result == 0) {
            result = Integer.compare(a.determineTeamNumber(), b.determineTeamNumber());
         }
         return result;
      };
   }


   public static Comparator<TeamData> getNextYearsLeagueComparator() {
      return (a, b) -> {
         int result = Series.getSortComparator().compare(a.series, b.series);
         if (result == 0) {
            result = a.determineClub().compareTo(b.determineClub());
         }
         if (result == 0) {
            result = Integer.compare(a.determineNextYearsLeague(), b.determineNextYearsLeague());
         }
         if (result == 0) {
            result = Integer.compare(a.determineTeamNumber(), b.determineTeamNumber());
         }
         return result;
      };
   }

   public static Comparator<TeamData> getAdditionalPromotionsComparator() {
      return (a, b) -> {
         int result = Integer.compare(a.clickTtRank, b.clickTtRank);
         if (result == 0) {
            result = Double.compare(b.leagueRankingIndex, a.leagueRankingIndex);
         }
         return result;
      };
   }

   public static Comparator<TeamData> getAdditionalRelegationsComparator() {
      return (a, b) -> {
         int result = Integer.compare(b.clickTtRank, a.clickTtRank);
         if (result == 0) {
            result = -Double.compare(b.leagueRankingIndex, a.leagueRankingIndex);
         }
         return result;
      };
   }

   public static Comparator<TeamData> getOmittableRelegationsComparator() {
      return (a, b) -> {
         int result = Boolean.compare(a.isRelegatedRegularly, b.isRelegatedRegularly);
         if (result == 0) {
            result = Double.compare(b.leagueRankingIndex, a.leagueRankingIndex);
         }
         return result;
      };
   }

}
